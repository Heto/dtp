﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pack : Item {
    public enum Kinds
    {
        Healing,
        Drug,
        Strength,
        Agility,
        Greed,
        Transformation,
        SkeletonKey,
        Castle,
        Grenade,
        Mine,
        HeavyMine,
        Turret
    }

    public Kinds kind;

    public int numberOfItem = 0;
    public PerksManager perks;

    // public GameObject getGrenade;

    void Start()
    {
        perks = GameObject.Find("Player").GetComponent<PerksManager>();
    }

    public override void Use()
    {
        switch (kind)
        {
            case Kinds.Healing: // HP 일정량 즉시 회복
                {
                    // UI Pref 삭제
                    if (InstSlotUI != null)
                        Destroy(InstSlotUI);

                    gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    player.item = null;
                    player.hp += 40;
                    Destroy(gameObject);
                    break;
                }
            case Kinds.Drug: // 5초간 조작키가 반대로 바뀌지만 지속시간 동안 HP 점차 회복
                {
                    // UI Pref 삭제
                    if (InstSlotUI != null)
                        Destroy(InstSlotUI);

                    gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    StartCoroutine(DrugOn());
                    break;
                }
            case Kinds.Strength: // 10초간 해치 여는 속도 증가, 변신상태 공격력 증가
                {
                    // UI Pref 삭제
                    if (InstSlotUI != null)
                        Destroy(InstSlotUI);

                    gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    StartCoroutine(StrengthOn());
                    break;
                }
            case Kinds.Agility: // 10초간 이동속도 증가, 구르기 충전속도 증가
                {
                    // UI Pref 삭제
                    if (InstSlotUI != null)
                        Destroy(InstSlotUI);
                    player.agilityUsed = true;
                    gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    StartCoroutine(Agility());
                    break;
                }
            case Kinds.Greed: // 들고 있으면 에너지 획득량 증가, 사용시 에너지 소량 획득
                {
                    // UI Pref 삭제
                    if (InstSlotUI != null)
                        Destroy(InstSlotUI);

                    CarryOff(); // 지속되는 아이템 종료
                    gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    player.item = null;
                    player.energy += 30;
                    Destroy(gameObject);
                    break;
                }
            case Kinds.Transformation: // 현재 들고있는 무기를 랜덤한 다른 무기로 바꿈
                {
                    // UI Pref 삭제
                    if (InstSlotUI != null)
                        Destroy(InstSlotUI);

                    gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    player.item = null;

                    WeaponSlot.TransformWeapon();

                    Destroy(gameObject);
                    break;
                }
            case Kinds.SkeletonKey: // 해치문 한 개/상자를 선택해 즉시 엶
                {
                    Hatch[] hatches = GameObject.FindObjectsOfType<Hatch>();
                    foreach (Hatch hc in hatches)
                    {
                        if (hc.tryHatchOpen)
                        {
                            // UI Pref 삭제
                            if (InstSlotUI != null)
                                Destroy(InstSlotUI);

                            gameObject.GetComponent<SpriteRenderer>().enabled = false;
                            player.item = null;
                            hc.timer = 0;
                            hc.tryHatchOpen = false;
                            Destroy(gameObject);
                            break;
                        }
                    }
                    // 상자는 보류
                    break;
                }
            case Kinds.Castle: // 바리케이드 한 개를 선택해 내구도를 100으로 만듦
                {
                    Barricade[] baricades = GameObject.FindObjectsOfType<Barricade>();
                    foreach (Barricade brcd in baricades)
                    {
                        if (brcd.tryBaricadeReinforce)
                        {
                            // UI Pref 삭제
                            if (InstSlotUI != null)
                                Destroy(InstSlotUI);

                            gameObject.GetComponent<SpriteRenderer>().enabled = false;
                            player.item = null;
                            brcd.hp = 200;
                            brcd.tryBaricadeReinforce = false;
                            Destroy(gameObject);
                            break;
                        }
                    }
                    break;
                }
            case Kinds.Grenade:
                {
                    if (perks.perk_UtilityBelt)
                    {
                        numberOfItem++;                        
                        GameObject temp1 = Resources.Load("Grenade") as GameObject;
                        player = GameObject.FindObjectOfType<PlayerController>();
                        Instantiate(temp1, player.transform.position, Quaternion.identity);
                        if (numberOfItem >= 5)
                        {
                            if (InstSlotUI != null)
                                Destroy(InstSlotUI);
                            Destroy(gameObject);
                        }                       
                    }
                    else
                    {
                        numberOfItem++;
                        GameObject temp1 = Resources.Load("Grenade") as GameObject;
                        player = GameObject.FindObjectOfType<PlayerController>();
                        Instantiate(temp1, player.transform.position, Quaternion.identity);
                        if (numberOfItem >= 3)
                        {
                            if (InstSlotUI != null)
                                Destroy(InstSlotUI);
                            Destroy(gameObject);
                        }
                    }

                    break;

                }
            case Kinds.Mine:
                {
                    if (perks.perk_UtilityBelt)
                    {                      
                        numberOfItem++;
                        GameObject temp1 = Resources.Load("claymore") as GameObject;
                        player = GameObject.FindObjectOfType<PlayerController>();
                        Instantiate(temp1, player.transform.position - new Vector3(0, 0.22f, 0), Quaternion.identity);
                        //temp1.transform.localScale = new Vector2(0.8f, 0.8f);
                        if (numberOfItem >= 5)
                        {
                            if (InstSlotUI != null)
                                Destroy(InstSlotUI);
                            Destroy(gameObject);
                        }
                    }
                    else
                    {
                        numberOfItem++;
                        GameObject temp1 = Resources.Load("claymore") as GameObject;
                        player = GameObject.FindObjectOfType<PlayerController>();
                        Instantiate(temp1, player.transform.position - new Vector3(0, 0.22f, 0), Quaternion.identity);
                        if (numberOfItem >= 3)
                        {
                            if (InstSlotUI != null)
                                Destroy(InstSlotUI);
                            Destroy(gameObject);
                        }
                    }
                    
                    break;
                }
            case Kinds.HeavyMine:
                {
                    if (perks.perk_UtilityBelt)
                    {
                        numberOfItem++;
                        GameObject temp1 = Resources.Load("antiTankMine") as GameObject;
                        player = GameObject.FindObjectOfType<PlayerController>();
                        Instantiate(temp1, player.transform.position - new Vector3(0, 0.27f, 0), Quaternion.identity);
                        if (numberOfItem >= 4)
                        {
                            if (InstSlotUI != null)
                                Destroy(InstSlotUI);
                            Destroy(gameObject);
                        }
                    }
                    else
                    {
                        numberOfItem++;
                        GameObject temp1 = Resources.Load("antiTankMine") as GameObject;
                        player = GameObject.FindObjectOfType<PlayerController>();
                        Instantiate(temp1, player.transform.position - new Vector3(0, 0.27f, 0), Quaternion.identity);
                        if (numberOfItem >= 2)
                        {
                            if (InstSlotUI != null)
                                Destroy(InstSlotUI);
                            Destroy(gameObject);
                        }
                    }
                    
                    break;
                }
            case Kinds.Turret:
                {
                    if (InstSlotUI != null)
                        Destroy(InstSlotUI);
                    GameObject temp1 = Resources.Load("TurretBody") as GameObject;
                    player = GameObject.FindObjectOfType<PlayerController>();
                    Instantiate(temp1, player.transform.position-new Vector3(0,0.073f,0), Quaternion.identity);
                    Destroy(gameObject);
                    break;
                }
        }

    }

    IEnumerator DrugOn()
    {
        player.item = null;
        
        player.moveSpeed *= -1;
        for (int i = 0; i < 5; i++)
        {
            player.hp += 5;
            yield return new WaitForSeconds(1.0f);
        }
        player.moveSpeed *= -1;

        Destroy(gameObject);
    }

    IEnumerator StrengthOn()
    {
        player.item = null;
        player.hatchOpenSpeed = player.hatchOpenSpeed + 1.0f;
        player.transformDamage = 2.0f;
        yield return new WaitForSeconds(10.0f);
        player.transformDamage = 1.0f;
        if (perks.perk_Muscular)
            player.hatchOpenSpeed = 1.0f;
        else
            player.hatchOpenSpeed = 0.5f;

        Destroy(gameObject);
    }

    IEnumerator Agility()
    {
        player.item = null;

        player.moveSpeed = (player.moveSpeed > 0) ? player.moveSpeed += 2 : player.moveSpeed -= 2;
        yield return new WaitForSeconds(10.0f);
        player.moveSpeed = (player.moveSpeed > 0) ? player.moveSpeed -= 2 : player.moveSpeed += 2;
        player.agilityUsed = false;
        Destroy(gameObject);
    }

    public override void CarryOn()
    {
        if (kind == Kinds.Greed)  // 들고 있으면 에너지 획득량 증가, 사용시 에너지 소량 획득
        {
            player.amountObtainEnergy = 5;
        }
    }

    public override void CarryOff()
    {
        if (kind == Kinds.Greed)
        {
            player.amountObtainEnergy = 0;
        }
    }
}