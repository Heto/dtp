﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyUI : MonoBehaviour
{

	public Text energy;
	public PlayerController player;

	// Use this for initialization
	void Start()
	{
		energy = GetComponent<Text>();

	}

	// Update is called once per frame
	void Update()
	{
		player = GameObject.Find("Player").GetComponent<PlayerController>();
		energy.text = player.energy.ToString();
	}
}
