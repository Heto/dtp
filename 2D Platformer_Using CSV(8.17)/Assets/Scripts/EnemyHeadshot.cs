﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHeadshot : MonoBehaviour {

	public EnemyController enemy;
	public GameObject bulletEffect;
	// Use this for initialization
	void Start()
	{
		enemy = transform.parent.gameObject.GetComponent<EnemyController>();
	}

	// Update is called once per frame
	void Update()
	{

	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Bullet")
		{
			if (WeaponSlot.GetWeaponIdx() == 0 || WeaponSlot.GetWeaponIdx() == 6)
				enemy.hp -= enemy.weapon.damage * 3;
			else
				enemy.hp -= enemy.weapon.damage * 2;
			Instantiate(bulletEffect, col.transform.position, col.transform.rotation);
			//Destroy(col.gameObject);
			StartCoroutine(HitEffect());
			Debug.Log("HeadShot");
		}

	}

	IEnumerator HitEffect()
	{
		Renderer rend = transform.parent.gameObject.GetComponent<Renderer>();

		rend.material.color = Color.red;
		yield return new WaitForSeconds(0.04f);
		rend.material.color = Color.white;
		yield return new WaitForSeconds(0.04f);

	}
}
