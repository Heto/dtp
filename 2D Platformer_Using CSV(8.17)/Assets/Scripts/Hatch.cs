﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hatch : MonoBehaviour
{
    public PerksManager perks;
	public Image doorBar;
	public float timer;
	public Upstair upstair;
	public PlayerController player;
	public float openSpeed;
    public Animator anim;
    public bool hatchOpened = false;
    public bool setActive = false;
    public WaveSpawner getWaveSpawner;
    public bool firstOpened = false;

    /***** 성민 : 시작 *****/
    public bool tryHatchOpen;
    /***** 성민 : 종료 *****/

    // Use this for initialization
    void Start()
	{
		openSpeed = 2f;
        anim = GetComponent<Animator>();
        perks = GameObject.Find("Player").GetComponent<PerksManager>();
    }

	// Update is called once per frame
	void Update()
	{

		doorBar.fillAmount = timer / 10;
		player = GameObject.Find("Player").GetComponent<PlayerController>();
        getWaveSpawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<WaveSpawner>();

        if (timer <= 0)
        {
            hatchOpened = true;
            anim.SetTrigger("open");

        }

    }

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
            if (Input.GetKey(KeyCode.E) && hatchOpened == false)
			{
				timer -= Time.deltaTime * player.hatchOpenSpeed;
			}

            if (Input.GetKeyDown(KeyCode.E))
            {
                if (hatchOpened)
                {
                    if (firstOpened == false)
                    {
                        FirstOpened();
                    }

                }                
                
                if (setActive == true)
                {
                    player.takeUpstair = true;

                }

            }

            /***** 성민 : 시작 *****/
            if (Input.GetKey(KeyCode.F))
                tryHatchOpen = true;
            else
                tryHatchOpen = false;
            /***** 성민 : 종료 *****/
        }
    }

    void FirstOpened()
    {
        player.oxygenpoint = 100f;
        getWaveSpawner.GetComponent<WaveSpawner>().spawnMax += 2;
        if (perks.perk_Brave)
            player.braveCount += 3;
        player.hatchOpened = true;
        setActive = true;
        firstOpened = true;
    }
            
}
