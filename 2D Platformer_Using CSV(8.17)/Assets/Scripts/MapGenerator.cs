﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

    public GameObject bossFloor;
    public GameObject insfloor; // block
    public GameObject leftDoor;
    public GameObject rightDoor;
   // public GameObject itemBox;
    public GameObject downStair;
    public GameObject upStair;
    public GameObject hatch;
    public GameObject playerBlockLeft;
    public GameObject playerBlockRIght;
    /*
    public Transform leftDoorpos;
    public Transform RightDoorpos;
    public Transform itemBoxpos;
    public Transform downStairpos;
    public Transform upStairpos;
    public Transform hatchpos;
    public Transform playerBlockLeftpos;
    public Transform playerBlockRightpos;
    */

    public int floorNum; // 10 floor
    private int currentFloor; // create now floor
    //private float itemBoxPercent; // item boxPercent = 0;
    public Transform currentFloorPos;




    //가중치 
    //floor position different = 4
    private float weightPointNum;
    private List<Transform> a;
    private List<Transform> spawnList;

    //private Transform[] List;

    public GameObject perkBox;
    private float perkBoxPercent=0.45f; //0.3
    private int perkBoxAmount = 3;

    public GameObject ItemBox;
    private float ItemBoxPercent=0.45f; //0.3


    public GameObject weaponBox;
    private float weaponBoxPercent=0.9f; //0.3
    private int weaponBoxAmount = 2;

    // Use this for initialization
    void Start () {
        a = new List<Transform>();
        spawnList = new List<Transform>();

       // currentFloorPos.transform.position = new Vector3(-1.1f, -4f, 0f); // 1 floor pos
        weightPointNum = 0;
        currentFloor = 0;
        //floorNum = 10; 
        StartCoroutine(mapgenerator());
        if(currentFloor == floorNum+2)
        {
            StopAllCoroutines();
        }


	}
    public List<Transform> getSpawnPointList()
    {
        return spawnList;
    }
    IEnumerator mapgenerator()
    {
        while (currentFloor < floorNum)
        {
            //create floor
            GameObject getobj = Instantiate(insfloor, currentFloorPos.transform.position, Quaternion.identity);
            a = getobj.GetComponent<GetTransFloor>().GetTransList();
            // 0 = leftbarricade
            // 1 = rightbarricade
            // 2 = hatch
            // 3 = upstair
            // 4 = downstair
            // 5 = itembox
            // 6 = spawn left
            // 7 = spawn right
            // 8 = block left
            // 9 = block right
            Instantiate(leftDoor, new Vector3(a[0].transform.position.x,a[0].transform.position.y+weightPointNum,a[0].transform.position.z), Quaternion.identity);
            Instantiate(rightDoor, new Vector3(a[1].transform.position.x, a[1].transform.position.y + weightPointNum, a[1].transform.position.z), Quaternion.identity);
            Instantiate(hatch, new Vector3(a[2].transform.position.x, a[2].transform.position.y + weightPointNum, a[2].transform.position.z), Quaternion.identity);

            Instantiate(upStair, new Vector3(a[3].transform.position.x, a[3].transform.position.y + weightPointNum, a[3].transform.position.z), Quaternion.identity);


            //1층 제외 생성
            if (currentFloor != 0)
            {
                Instantiate(downStair, new Vector3(a[4].transform.position.x, a[4].transform.position.y + weightPointNum, a[4].transform.position.z), Quaternion.identity);
            }

            float boxP = Random.Range(0.0f, weaponBoxPercent);
            if (0.0f <= boxP && boxP < perkBoxPercent) // 0~0.29  30%   perkbox
            {
                if (perkBoxAmount == 0)
                {
                    Instantiate(ItemBox, new Vector3(a[5].transform.position.x + Random.Range(-2.0f, 2.0f), a[5].transform.position.y + weightPointNum + 0.208f, 0.1f), Quaternion.identity);
                }
                else
                {
                    //2~3개
                    perkBoxPercent = 0;
                    Instantiate(perkBox, new Vector3(a[5].transform.position.x + Random.Range(-2.0f, 2.0f), a[5].transform.position.y + weightPointNum - 0.1f, 0.1f), Quaternion.identity);
                    perkBoxAmount -= 1;
                   
                }
            }


            else if (perkBoxPercent <= boxP && boxP < ItemBoxPercent) // 0.3 ~ 0.59 30%    itembox
            {
                //기본 아이템상자
                Instantiate(ItemBox, new Vector3(a[5].transform.position.x + Random.Range(-2.0f, 2.0f), a[5].transform.position.y + weightPointNum +0.208f, 0.1f), Quaternion.identity);
               
                perkBoxPercent += 0.03f;
                if (currentFloor > 10)
                {
                    weaponBoxPercent += 0.3f;
                }
            }


            else if (ItemBoxPercent <= boxP && boxP < weaponBoxPercent) // 0.6 ~ 0.90 30%   weaponbox
            {
                if (weaponBoxAmount == 0)
                {
                    Instantiate(ItemBox, new Vector3(a[5].transform.position.x + Random.Range(-2.0f, 2.0f), a[5].transform.position.y + weightPointNum + 0.208f, 0.1f), Quaternion.identity);
                }
                else
                {
                    // 최대2
                    weaponBoxPercent = ItemBoxPercent;
                    Instantiate(weaponBox, new Vector3(a[5].transform.position.x + Random.Range(-2.0f, 2.0f), a[5].transform.position.y + weightPointNum + 0.198f, 0.1f), Quaternion.identity);
                    weaponBoxAmount -= 1;
                }
            }
            if(weaponBoxAmount==2)
            {
                weaponBoxPercent += 0.2f;
            }

           // Debug.Log(boxP);
           

            /*
            //box percent 
            if (Random.Range(0.0f, 0.1f) < itemBoxPercent)
            {
                //Instantiate(itemBox, new Vector3(a[5].transform.position.x, a[5].transform.position.y + weightPointNum, 0.1f), Quaternion.identity);

                itemBoxPercent = 0;
            }
            else
            {
                itemBoxPercent += 0.1f;
            }
            */
            spawnList.Add(a[6]);
            spawnList.Add(a[7]);

            Instantiate(playerBlockLeft, new Vector3(a[8].transform.position.x, a[8].transform.position.y, a[8].transform.position.z), Quaternion.identity);
            Instantiate(playerBlockRIght, new Vector3(a[9].transform.position.x, a[9].transform.position.y, a[9].transform.position.z), Quaternion.identity);
           
            
            //////////////////////create one floor/////////////////////////
            currentFloorPos.transform.position = new Vector3(currentFloorPos.transform.position.x, currentFloorPos.transform.position.y + 1.35f, currentFloorPos.transform.position.z);

            currentFloor++;
            //weightPointNum += 1f;
        }
        if (currentFloor == floorNum)
        {
            Instantiate(bossFloor, currentFloorPos.transform.position, Quaternion.identity);
            currentFloor++;
        }
        yield return null;
    }


}
