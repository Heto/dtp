﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMeleeAttack : MonoBehaviour
{
    private float attackdelay; // attack first delay time 
    private float attackCd; // attack cool down

    public Collider2D attackTrigger;
    public float getdelay; // get delay time
    public float getCd; // get cool down
    public float condition; // 공격 조건(거리)

    public float getdashdelay;
    public float getdashCd;
    public float getDashPower;

    private float dashpower;
    private Rigidbody2D rb;


    //public bool dashOn;
    private float timer;


    public GameObject EnemyBullet; // bullet
    public Transform firePoint; // bullet shoot position


    [HideInInspector]
    public bool AttackStart;
    public bool conditionOK;
    public bool CdOK;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        attackTrigger.enabled = false;
        attackdelay = getdelay;
        dashpower = getDashPower;
        attackCd = getCd;
        timer = 0.6f;
        AttackStart = false;
        conditionOK = false;
        CdOK = false;
        //dashOn = false;

    }
    /*
    public void AnimManager(Animator anim)
    {
        anim.SetBool("attack", true);
    }*/


    /// 좀비 노말의 공격 타임라인
    /// 이동false, 공격true -> 애니메이션 실행 -> 콜라이더생성 -> 콜라이더삭제 -> 애니메이션 취소 -> 이동 true, 공격false, 쿨타임,딜레이,사거리 false


    ///빠른 좀비의 공격 타임라인
    /// 이동false, 공격 true -> 애니메이션 실행 -> 대쉬(addforce) -> 콜라이더 생성 -> 콜라이더삭제 -> 대쉬 cancel -> 애니메이션끝 -> 이동true, 공격false, 쿨타임,딜레이,사거리,dashpower 초기화 및 false



    public void CreateAttackTriggerMelee()
    {
        //all zombie 공격 콜라이더 생성
        attackTrigger.enabled = true;
    }
    public void DeleteAttackTriggerMelee()
    {
        //all zombie 공격 콜라이더 삭제
        attackTrigger.enabled = false;

    }
    public void MeleeAttackAnim(Animator anim)
    {
        //all zombie 공격 애니메이션실행, 이동멈춤
        anim.SetBool("attack", true);
        anim.SetBool("move", false);

    }

    public void DeleteAttackAnim(Animator anim)
    {
        //Debug.Log("1212");
        //all zombie 공격 애니메이션끝, 쿨타임재설정, 딜레이재설정, 조건 false, 이동가능
        AttackStart = false;
        anim.SetBool("attack", false);
        anim.SetBool("move", true);
        attackCd = getCd;
        attackdelay = getdelay;
       // dashpower = getDashPower;
        conditionOK = false;

    }


    /// Dash
    public void MeleeDashAttackAnim(Animator anim)
    {
        anim.SetBool("attackDash", true);
        anim.SetBool("move", false);
    }
    public void DeleteDashAttackAnim(Animator anim)
    {
        AttackStart = false;
        anim.SetBool("attackDash", false);
        anim.SetBool("move", true);
        attackCd = getCd;
        attackdelay = getdelay;
        dashpower = getDashPower;
        conditionOK = false;
    }
    public void DashStartAnim(bool isleft)
    {
        if (isleft)
            rb.AddForce(Vector2.right * dashpower);
        else
            rb.AddForce(Vector2.left * dashpower);

        //dashpower -= Time.deltaTime * 1.2f;
    }
    public void TestDash(bool isleft)
    {
        if (isleft)
            rb.AddForce(Vector2.right * dashpower);
        else
            rb.AddForce(Vector2.left * dashpower);

    }

    public void DashEndAnim()
    {
        rb.velocity = Vector2.zero;
    }
    
    public void MeleeAttack()
    {
  
        if (attackdelay > 0)
        {
           // anim.SetBool("attack2", true);
           // anim.SetBool("move", false);
            attackdelay -= Time.deltaTime;
        }
        else
        {

            if (timer > 0)
            {
                attackTrigger.enabled = true;
                timer -= Time.deltaTime;
            }
            else
            {
                attackTrigger.enabled = false;
                attackCd = getCd;
                attackdelay = getdelay;
                timer = 0.6f;
                AttackStart = false;
                conditionOK = false;
               // anim.SetBool("attack2", false);
               // anim.SetBool("move", true);
            }
        }
    }
    public void DashAttack(bool isleft)
    {
        if (attackdelay > 0)
        {
            attackdelay -= Time.deltaTime;
        }
        else
        {
            if (isleft)
                rb.AddForce(Vector2.right * dashpower);
            else
                rb.AddForce(Vector2.left * dashpower);
            attackTrigger.enabled = true;
            if (timer > 0)
            {
                timer -= Time.deltaTime;
                dashpower -= Time.deltaTime * 1.2f;
            }
            else
            {
                attackTrigger.enabled = false;
                attackCd = getdashCd;
                attackdelay = getdashdelay;
                timer = 0.3f;
                dashpower = 30;
                AttackStart = false;
                conditionOK = false;
            }
        }
    }

    


    public void ShootAnimStart(Animator anim)
    {
        anim.SetBool("shoot", true);
        anim.SetBool("move", false);

    }
    public void ShootAnimEnd(Animator anim)
    {
        anim.SetBool("shoot", false);
        anim.SetBool("move", true);
        attackCd = getCd;
        conditionOK = false;
        AttackStart = false;
    }

    public void ShootingBullet(bool IsLeft)
    {
        GameObject bullet = (GameObject)Instantiate(EnemyBullet, firePoint.position, firePoint.rotation);
        if (IsLeft)
        {
            bullet.GetComponent<Rigidbody2D>().velocity = Vector2.right * 2;

        }
        else
        {
            bullet.GetComponent<Rigidbody2D>().velocity = Vector2.left * 2;

        }
    }






    public void CheckingAttack(float distance)
    {
        CheckCondition(distance);
        CheckCd();
        if (CdOK && conditionOK)
        {

            AttackStart = true;

        }
    }


    public void CheckCondition(float distance) // 거리측정
    {
        if (condition > distance)
        {

            conditionOK = true;
        }

    }
    public void CheckCd() //쿨타임 측정
    {
        if (attackCd >= 0)
        {
            CdOK = false;
            attackCd -= Time.deltaTime;
        }
        else
        {
            CdOK = true;
        }
    }
}
