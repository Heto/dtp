﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public static class Rigidbody2DExtension
{
	public static void AddExplosionForce(this Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius)
	{
		var dir = (body.transform.position - explosionPosition);
		float wearoff = 1 - (dir.magnitude / explosionRadius);
		body.AddForce(dir.normalized * explosionForce * wearoff);
	}

	public static void AddExplosionForce(this Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius, float upliftModifier)
	{
		var dir = (body.transform.position - explosionPosition);
		float wearoff = 1 - (dir.magnitude / explosionRadius);
		Vector3 baseForce = dir.normalized * explosionForce * wearoff;
		body.AddForce(baseForce);

		float upliftWearoff = 1 - upliftModifier / explosionRadius;
		Vector3 upliftForce = Vector2.up * explosionForce * upliftWearoff;
		body.AddForce(upliftForce);
	}
}


public class BoomTrigger : MonoBehaviour
{
    private AudioSource source;
    public AudioClip boomSound;
	public float destroytime;
	public int type; // 1==from enemy, 2==from player
	
					 // Use this for initialization
	void Start()
	{
        if (boomSound != null)
            source = GetComponent<AudioSource>();

        if(source!=null)
        {
            source.PlayOneShot(boomSound,1.0f);
        }
        //Boom();
        Destroy(this.gameObject, destroytime);
	}
	private void Boom()
	{
		Collider2D[] List = Physics2D.OverlapCircleAll(transform.position, 2f);

		foreach (Collider2D hit in List)
		{
			Rigidbody2D rigid = hit.gameObject.GetComponent<Rigidbody2D>();
			if (rigid != null)
			{
				Debug.Log("BOOM RUNNING");
				rigid.AddExplosionForce(100f, this.transform.position, 5f, 10f);
				//Rigidbody2DExtension.AddExplosionForce(rigid, 2f, transform.position, 2f);
			}
		}
	}
    public void BoomEnd()
    {
        Destroy(gameObject);
    }
	private void OnTriggerEnter2D(Collider2D col)
	{
		if (type == 1)
		{
			if (col.tag == "Player")
			{

                //col.GetComponent<PlayerController>().GetDamage(100f);

				Debug.Log("hit player from Boom");
			}
		}
		else if (type == 2)
		{
			

		}
        else if (type == 3)//from boss
        {
            if (col.tag == "Player")
            {

                col.GetComponent<PlayerController>().GetDamage(10f);

                Debug.Log("hit player from Boom");
            }
        }
	}




}
