﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CsvInfo : MonoBehaviour
{

	public float rate;
	public int round;
	public float reloadTime;
	public float recoil;
	public float maxRecoil;
	public float minRecoil;
	public float restoreRecoil;
	public float damage;
    
	void Start()
	{
		List<Dictionary<string, object>> data = CsvParser.Read("weaponInfo");

		//플레이어의 총이 무엇인지에 따라 받아오는 정보가 다르도록 함.
		var i = WeaponSlot.GetWeaponIdx();

		rate = (float)data[i][" RATE"];
		round = (int)data[i][" ROUND"];
		reloadTime = (float)data[i][" RELOADTIME"];
		recoil = (float)data[i][" RECOIL"];
		maxRecoil = (float)data[i][" MAXRECOIL"];
		minRecoil = (float)data[i][" MINRECOIL"];
		restoreRecoil = (float)data[i][" RESTORERECOIL"];
		damage = (float)data[i][" DAMAGE"];
	}

	// Update is called once per frame
	void Update()
	{

	}
}
