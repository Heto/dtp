﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPattern : MonoBehaviour
{
    public Weapon weapon;
    public PerksManager perks;
    public GameObject bulletEffect;
   // public GameObject doorblock;

    public float hp = 500;
  //  private Rigidbody2D rb2D;

    //Boss Pattern - Bullet Pattern
    public GameObject bullet;
    private int bulletPatternAmount; // 탄막개수
    private float bulletSpeed = 5f;
    private float radius = 1f;
    //fury
    private int getBulletPatternAmount=9; // 7-> 12
    private float getBulletSpeed = 5f;  // 5 -> 6

    //Boss Pattern - EarthQuake Pattern
    public List<Transform> earthPos;
    private Transform selectedPos;
    public GameObject Aura;
    public GameObject earthQuake;
    private int auraCount;
    private float aura_delay;
    private int earthCount;
    //fury
    private int getAuraCount = 2;  // 2 -> 1
    private float getAuradelay = 0.7f; // .7 -> .5

    //Boss Pattern - MeleeAttack Pattern
    public Collider2D meleeTrigger;
    public float meleeDelay=1f;
    public float meleeAfterDelay =0.6f;
    private float meleeSpeed=1f;
    private float step = 0.5f;
    private Quaternion meleeEnd = Quaternion.identity;
    //fury
    private float getMeleeDelay = 1f; // -> 1 -> 0.8
    private float getMeleeAfterDelay = 0.6f; // 0.6 -> 0.3
    private float getMeleeSpeed = 1f; // 1 -> 1.2
    private float getStep = 0.5f;  // 0.5 -> 0.6


    //Boss Pattern - Tentacle Pattern
    public List<Transform> tentaPos;
    public GameObject tentaAura;
    private bool isTentacle = false;
    private float tentacleTerm = 0.2f;
    private int k = 0;
    //fury
    private float getTentacleTerm = 0.2f; // 0.2 -> 0.18
    


    //Boss Pattern - Invincibility Pattern
    public GameObject bossGuard;
    public GameObject bossEye;
    private float bulletTerm=1.5f;

    //Boss Pattern Manager
    private int patternManager = 0; //  0 = rest , 1 = bullet, 2 = earthquake, 3 = meleeattack, 4 = tentacle, 5 invincibility,  ,10 = after delay
    private float dist = 0;
    private float patternAfterDelayTimer =1.0f;
    private bool furyInitCheck = false;
    private float furyTentaTimer = 2.0f;

    private Animator anim;
    public GameObject BulletPos;


    // Use this for initialization
    void Start()
    {
        perks = GameObject.Find("Player").GetComponent<PerksManager>();
        anim = this.GetComponent<Animator>();
      //  shuffle(tantaPos);
        meleeEnd.eulerAngles = new Vector3(0, 0, -125);
        selectedPos = null;
       // rb2D = GetComponent<Rigidbody2D>();
        Init();

    }
    void LateUpdate()
    {
        weapon = WeaponSlot.GetWeapon();
    }
    void Init()
    {
        bulletSpeed = getBulletSpeed;
        bulletPatternAmount = getBulletPatternAmount;
        //둘다 direct

        auraCount = getAuraCount;
        aura_delay = getAuradelay;
        earthCount = 3;
        //init

        meleeDelay = getMeleeDelay;
        meleeAfterDelay = getMeleeAfterDelay;
        meleeSpeed = getMeleeSpeed;
        step = getStep;
        //step 은 direct // 나머진 init

        tentacleTerm = getTentacleTerm;
        //init

    }
    void furyinit()
    {
        getBulletSpeed = 6;
        getBulletPatternAmount = 13;

        bulletSpeed = getBulletSpeed;
        bulletPatternAmount = getBulletPatternAmount;
        ///////////////////////////////////////////////////////////
        getAuraCount = 1;
        getAuradelay = 0.7f;

        auraCount = getAuraCount;
        aura_delay = getAuradelay;
        ///////////////////////////////////////////////////////////
        getMeleeDelay = 0.8f;
        getMeleeAfterDelay = 0.3f;
        getMeleeSpeed = 1.2f;
        getStep = 0.6f;

        meleeDelay = getMeleeDelay;
        meleeAfterDelay = getMeleeAfterDelay;
        meleeSpeed = getMeleeSpeed;
        step = getStep;
        //////////////////////////////////////////////////////////////
        getTentacleTerm = 0.18f;
        tentacleTerm = getTentacleTerm;

        furyInitCheck= true;
       
    }
    void PatternBullet()
    {
        float angleStep = 80 / bulletPatternAmount;
        float angle = 100;

        for (int i = 0; i < bulletPatternAmount - 1; i++)
        {
            float dirPositionX = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180) * radius;
            float dirPositionY = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180) * radius;

            Vector3 bulletVector = new Vector3(dirPositionX, dirPositionY, 0);
            Vector3 moveDirection = (bulletVector - this.transform.position).normalized * bulletSpeed;

            GameObject tmpObj = Instantiate(bullet, BulletPos.transform.position, Quaternion.identity);
            tmpObj.GetComponent<Rigidbody2D>().velocity = new Vector3(moveDirection.x, moveDirection.y, 0);
            angle += angleStep;
            Destroy(tmpObj, 15f);
        }
        patternManager = 10;
    }
    void PatternBullet2()
    {
        float angleStep = 80 / bulletPatternAmount;
        float angle = 100;

        for (int i = 0; i < bulletPatternAmount - 1; i++)
        {
            float dirPositionX = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180) * radius;
        float dirPositionY = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180) * radius;

            Vector3 bulletVector = new Vector3(dirPositionX, dirPositionY,0);
            Vector3 moveDirection = (bulletVector - this.transform.position).normalized * bulletSpeed;

            GameObject tmpObj = Instantiate(bullet,BulletPos.transform.position, Quaternion.identity);
            tmpObj.GetComponent<Rigidbody2D>().velocity = new Vector3(moveDirection.x, moveDirection.y,0);
            angle += angleStep;
            Destroy(tmpObj, 15f);
        }
        patternManager = 10;

    }
    void EarthQuake()
    {
        if (earthCount > 0)
        {
            if (selectedPos == null)
            {
                //selec pos
                selectedPos = earthPos[(int)Random.Range(0, 3)];
            }
            //give aura
            if (auraCount > 0)
            {
                if (aura_delay > 0)
                {
                    aura_delay -= Time.deltaTime;
                }
                else
                {
                    //show aura  noramal = 2 ,  fury = 1
                    GameObject _aura = Instantiate(Aura, new Vector3(selectedPos.position.x, selectedPos.position.y, 0),Quaternion.identity);
                    Destroy(_aura, 0.5f);
                    aura_delay = 0.7f;
                    auraCount -= 1;
                }
            }
            //attack earthquake
            else
            {
                if (aura_delay > 0)
                {
                    aura_delay -= Time.deltaTime;
                }
                else
                {
                    Instantiate(earthQuake, new Vector3(selectedPos.position.x, selectedPos.position.y, 0), Quaternion.identity);

                    //if문 fury
                    auraCount = getAuraCount;
                    aura_delay = getAuradelay;
                    selectedPos = null;
                    earthCount--;
                }
            }
        }
        else
        {
            earthCount = 3;
            patternManager = 10;
        }


    }
    public void AnimTureMelee()
    {
        meleeTrigger.enabled = true;
        
    }
    public void AnimFalseMelee()
    {
        meleeTrigger.enabled = false;
    }
    public void AnimEndMelee()
    {
        anim.SetBool("melee", false);
        patternManager = 10;
    }
    void MeleeAttack()
    {
        meleeTrigger.enabled = true;
        if(meleeDelay>0)
        {
            meleeDelay -= Time.deltaTime;
        }
        else
        {
            if(meleeEnd == meleeTrigger.transform.rotation)
            {
                if (meleeAfterDelay > 0)
                {
                    meleeAfterDelay -= Time.deltaTime;
                }
                else
                {
                    meleeAfterDelay = getMeleeAfterDelay;
                    meleeDelay = getMeleeDelay;
                    meleeTrigger.transform.rotation = new Quaternion(0, 0, 0,0);
                    meleeSpeed = getMeleeSpeed;
                    meleeTrigger.enabled = false;
                    patternManager = 10;
                }
            }
            else
            {
                //meleeTrigger.transform.rotation = Quaternion.Lerp(meleeTrigger.transform.rotation, new Quaternion(0, 0, -125, 0), Time.deltaTime * 20f);
                //meleeTrigger.transform.Rotate(0, 0, -1 * Time.deltaTime * 20.0f*meleeSpeed );
                meleeTrigger.transform.rotation = Quaternion.Slerp(meleeTrigger.transform.rotation, meleeEnd, Time.deltaTime * 0.5f*meleeSpeed);
                meleeSpeed += step;
            }
        }
    }
    void SumonTantacle()
    {
        if (!isTentacle)
        {
            Shuffle(tentaPos);
            isTentacle = true;
        }
        if(k<tentaPos.Count)
        {
            if(tentacleTerm>0)
            {
                tentacleTerm -= Time.deltaTime;
            }
            else
            {
                Instantiate(tentaAura, tentaPos[k].transform.position,Quaternion.identity);
                k++;
                tentacleTerm = getTentacleTerm;
            }
        }
        else
        {
            k = 0;
            isTentacle = false;
            patternManager = 10;
        }
        
    
        /*
        //깜빡이
        foreach (Transform pos in tentaPos)
        {
           tentaCoroutine[0]=StartCoroutine(TentaCycle(pos));
        }
        for (int i = 0; i < tentaPos.Count; i++)
        {
            if (TentaCycle(tentaPos[i])==10)
            { 

                }
        }
        //ㄱㄱㄱ
        */
    }
    /*
    IEnumerator TentaCycle(Transform pos)
    {
        //aura sumon
        GameObject tep = Instantiate(tentaAura, pos.transform.position, Quaternion.identity);
        Destroy(tep, 1.0f);
        yield return new WaitForSeconds(1.0f);
        GameObject temp = Instantiate(tentacle, pos.transform.position - new Vector3(0,-5.3f,0), Quaternion.identity);
        temp.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 1, 0);
        // temp.transform.position =Vector3.Lerp(temp.transform.position, new Vector3(temp.transform.position.x, temp.transform.position.y + 5.3f, temp.transform.position.z), Time.deltaTime * 0.5f * meleeSpeed);
        Destroy(temp, 2.0f);
        yield return new WaitForSeconds(2.0f);
        yield return 10;
    }*/
    
    void Shuffle(List<Transform> array)
    {
        int p = array.Count;
        for(int n = p-1; n>0; n--)
        {
            int r = Random.Range(0, n);
            Transform t = array[r];
            array[r] = array[n];
            array[n] = t;
        }

    }
    void InvincibilityPattern()
    {
        if (bossEye == null && bossGuard == null)
        {
            patternManager = 0;
        }
        anim.SetBool("rage", true);
            //가드
            bossGuard.SetActive(true);
        //약점소환
        bossEye.SetActive(true);
        //탄막만계속쏨
        if(bossEye != null && bossGuard != null)
        {
            if (bulletTerm > 0)
            {
                bulletTerm -= Time.deltaTime;
            }
            else
            {
                if (Random.Range(0, 2) >= 1)
                {
                    PatternBullet();
                }
                else
                {
                    PatternBullet2();
                }
                bulletTerm = 1.5f;
            }
        }


    }





    // Update is called once per frame
    void Update()
    {
        Debug.DrawLine(transform.position, GameObject.FindGameObjectWithTag("Player").transform.position);
        dist = Vector3.Distance(transform.position, GameObject.FindGameObjectWithTag("Player").transform.position);
       
       Debug.Log(dist);
        /*
        foreach (Transform array in tantaPos)
        {
            Debug.Log(array);
        }*/
        /*
        if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            iuiu = 1;
        }

        if(iuiu == 1)
        {
            EarthQuake();
        }

        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            iuiu = 2;
        }
        if(iuiu==2)
        {
            MeleeAttack();
        }

        if(Input.GetKeyDown(KeyCode.Keypad1))
        {

            PatternBullet();
        }
        else if(Input.GetKeyDown(KeyCode.Keypad2))
        {
            PatternBullet2();
        }
        else if(Input.GetKeyDown(KeyCode.Keypad3))
        {
            iuiu = 5;
        }
        if (iuiu == 3)
        {
            PatternBullet();
        }
        if (iuiu == 4)
        {
            PatternBullet2();
        }
        if(iuiu == 5)
        {
            SumonTantacle();
        }
        */
        //패턴변수 = null
        //패턴변수에 넣어줌

        if(patternManager==0)
        {
            SelectedPattern();
        }
        else
        {
            //Debug.Log("??");
            StartPattern(patternManager);
        }
        if(furyInitCheck && hp>0)
        {
            if(furyTentaTimer>0)
            {
                furyTentaTimer -= Time.deltaTime;
            }
            else
            {
                Instantiate(tentaAura, GameObject.FindGameObjectWithTag("Player").transform.position-new Vector3(0,0.3f,0), Quaternion.identity);
                furyTentaTimer = 3.0f;
            }
        }

        if(hp<=0)
        {
            anim.SetBool("die", true);
            meleeTrigger.enabled = false;
            patternManager = 10;
        }
        //if 패턴변수가 null이 아닐때 읽어서 실행
        //실행하고나서 후딜레이 -> 다시 패턴변수 null


    }
    public void Boss_Die()
    {
        Destroy(gameObject);
    }
    void SelectedPattern()
    {
        float selec = Random.Range(0, 1f);
        if (selec < 0.3f)
            //30% bullet
        {
            patternManager = 1;
        }
        else if (0.3f<=selec&&selec<0.4f)
        //10% EarthQuake
        {
            patternManager = 2;
        }
        else if (0.4f<=selec&&selec<0.8f)
            //40% MeleeAttack
        {
            if (dist < 2.4f)
            {
                patternManager = 3;
            }
            else
            {
                patternManager = 0;
            }
        }
        else if (0.8f<=selec&& selec<1.0f)
            //20% sumonTentacle
        {
            patternManager = 4;
        }

        if (hp <= 1500 && bossEye != null && bossGuard != null)
        {
            if (!furyInitCheck)
            {
                furyinit();
            }
            patternManager = 5;
        }

    }
    void StartPattern(int num)
    {
        if(num == 1)
        {
            if (Random.Range(0, 2) >= 1)
            {
                PatternBullet();
            }
            else
            {
                PatternBullet2();
            }
        }
        else if(num == 2)
        {
            EarthQuake();
        }
        else if(num == 3)
        {
            anim.SetBool("melee", true);
            //MeleeAttack();
        }
        else if(num == 4)
        {
            SumonTantacle();
        }
        else if ( num == 5)
        {
            InvincibilityPattern();
            //+광폭화
        }
        else if (num == 10)
        {
            PatternAfterDelay();
        }
    }
    void PatternAfterDelay()
    {
        if(patternAfterDelayTimer >0f)
        {
            patternAfterDelayTimer -= Time.deltaTime;
        }
        else
        {
            patternAfterDelayTimer = 1.0f;
            patternManager = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag == "Bullet")
        {
            if (WeaponSlot.GetWeaponIdx() == 0 && perks.perk_Hitman == true)
            {
                if (weapon.perk_Slayer_Check)
                    hp -= 2.0f * (weapon.damage + 10.0f);
                else
                    hp -= (weapon.damage + 20.0f);

                weapon.perk_Slayer_Check = false;
            }
            else if (WeaponSlot.GetWeaponIdx() == 6 && perks.perk_Hitman == true)
            {
                if (weapon.perk_Slayer_Check)
                    hp -= 2.0f * (weapon.damage + 10.0f);
                else
                    hp -= (weapon.damage + 50.0f);

                weapon.perk_Slayer_Check = false;
            }

            else
            {
                if (weapon.perk_Slayer_Check)
                    hp -= 2.0f * weapon.damage;

                else
                    hp -= weapon.damage;

                weapon.perk_Slayer_Check = false;
            }


            Instantiate(bulletEffect, col.transform.position, col.transform.rotation);
            StartCoroutine(HitEffect());
            Destroy(col.gameObject);
        }

        if (col.gameObject.tag == "ThirdArmBullet")
        {
            hp -= 20;
            Instantiate(bulletEffect, col.transform.position, col.transform.rotation);
            StartCoroutine(HitEffect());
            Destroy(col.gameObject);
        }
    }
    IEnumerator HitEffect()
    {
        Renderer rend = GetComponent<Renderer>();

        rend.material.color = Color.red;
        yield return new WaitForSeconds(0.04f);
        rend.material.color = Color.white;
        yield return new WaitForSeconds(0.04f);

    }
}
