﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController_Explosive : MonoBehaviour
{
    public PerksManager perks;
    public GameObject explosionEffect;
    public Weapon weapon;
    private float spd;

    // Use this for initialization
    void Start()
    {
        perks = GameObject.FindObjectOfType<PerksManager>();
        weapon = WeaponSlot.GetWeapon();
        spd = weapon.bulletSpeed * 1.2f;
    }

    // Update is called once per frame
    void Update()
    {
        if (perks.perk_BulletSpeedUp)
            transform.Translate(Vector3.right * Time.deltaTime * spd);
        else
            transform.Translate(Vector3.right * Time.deltaTime * weapon.bulletSpeed);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Ground")
        {
            if (weapon.perks.perk_Compensator == true)
            {
                weapon.BulletCount++;
            }
            Destroy(gameObject);
        }
        if (col.gameObject.tag == "Enemy")
        {
            Instantiate(explosionEffect, transform.position, col.transform.rotation);
            Destroy(gameObject);
        }

    }

    //void OnTriggerStay2D(Collider2D col)
    //{
    //    if (col.gameObject.tag == "Ground")
    //    {
    //        if (weapon.perks.perk_Compensator == true)
    //        {
    //            weapon.BulletCount++;
    //        }
    //        Destroy(gameObject);
    //    }
    //    if (col.gameObject.tag == "Enemy")
    //    {
    //        Instantiate(explosionEffect, transform.position, col.transform.rotation);
    //        Destroy(gameObject);
    //    }

    //}
}
