﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Barricade : MonoBehaviour
{
    public PlayerController player;
    public Image barriBar;
    public float hp;
    private Animator myAnim;
    public bool broken;
    public float fixSpeed;
    //Collider2D m_collider;

    /***** 성민 : 시작 *****/
    public bool tryBaricadeReinforce;
    /***** 성민 : 종료 *****/

    void Start()
    {
        myAnim = GetComponent<Animator>();
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        broken = false;
        //m_collider = GetComponent<Collider2D>();
        fixSpeed = 6f;

    }

    // Update is called once per frame
    void Update()
    {
        barriBar.fillAmount = hp / 15;

        if (hp > 15 && hp < 17)
            hp = 15;
        else if (hp < 0)
            hp = 0;

        myAnim.SetFloat("hpPercent", barriBar.fillAmount);
        if (hp > 17)
            myAnim.SetTrigger("reinforced");
        myAnim.SetFloat("hp", hp);

        if (barriBar.fillAmount <= 0f)
        {
            broken = true;
        }
        else
        {
            broken = false;
        }
    }
    public void ApplyDamage(float damage)
    {
        hp -= damage;
    }

    public bool brokenNow()
    {
        return broken;
    }
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                hp += Time.deltaTime * player.barricadeFixSpeed;
            }

            /***** 성민 : 시작 *****/
            if (Input.GetKey(KeyCode.F))
                tryBaricadeReinforce = true;
            else
                tryBaricadeReinforce = false;
            /***** 성민 : 종료 *****/
        }

    }

}
