﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBoxPrice : MonoBehaviour {

    public ItemBox itembox;
    public TextMesh text;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        text.text = itembox.price.ToString();
	}
}
