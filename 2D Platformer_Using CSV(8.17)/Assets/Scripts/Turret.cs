﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {

	public PerksManager perks;
	public GameObject bullet;
    public GameObject firePoint;
    public TurretRotation tr;
	public bool isReloading;
	private float reloadCount;
	public float fireRate;
    public float speed = 10f;

    public Transform muzzlePosition;
    public List<GameObject> muzzle;
    // Use this for initialization
    void Start () {
		perks = GameObject.Find("Player").GetComponent<PerksManager>();
		isReloading = false;
        reloadCount = 0f;
        
    }
	
	// Update is called once per frame
	void Update () {
		reloadCount += Time.deltaTime;

        FindClosestEnemy();
        FiretoEnemy();

        if (isReloading == true && reloadCount >= fireRate)
		{
			reloadCount = 0f;
			isReloading = false;
		}

		if (perks.perk_TurretMaster == true)
			fireRate = 0.2f;
	}

   

    void FiretoEnemy()
    {
        if (tr.enemyInRange && isReloading == false)
        {
            if (tr.colenemy.transform.position.x > transform.position.x)
            {
                Instantiate(bullet, firePoint.transform.position, transform.rotation);
            }

            else if (tr.colenemy.transform.position.x < transform.position.x)
            {
                Instantiate(bullet, firePoint.transform.position, transform.rotation);
            }
            GameObject tmpobj = Instantiate(muzzle[(int)Random.Range(0, muzzle.Count)], muzzlePosition.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
            Destroy(tmpobj, 0.05f);

            isReloading = true;
            reloadCount = 0f;
        }
    }

    void FindClosestEnemy()
    {
        if (tr.enemyInRange)
        {
            float distanceToClosestEnemy = Mathf.Infinity;
            EnemyController closestEnemy = null;
            EnemyController[] allEnemies = GameObject.FindObjectsOfType<EnemyController>();

            foreach (EnemyController currentEnemy in allEnemies)
            {
                float distanceToEnemy = (currentEnemy.transform.position - this.transform.position).sqrMagnitude;
                if (distanceToEnemy < distanceToClosestEnemy)
                {
                    distanceToClosestEnemy = distanceToEnemy;
                    closestEnemy = currentEnemy;
                }
            }

            if (closestEnemy != null)
            {
                Vector2 direction = closestEnemy.transform.position - transform.position;
                float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
                Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed * Time.deltaTime);

                if (angle > 0f && angle < 100f || angle < 0f && angle > -90f)
                {
                    transform.localScale = new Vector3(1f, 1f, 1f);
                }

                if (angle > 100f && angle < 180f || angle < -90f && angle > -180f)
                {
                    transform.localScale = new Vector3(1f, -1f, 1f);
                }
            }
        }


    }
}
