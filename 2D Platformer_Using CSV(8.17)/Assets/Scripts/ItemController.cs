﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemController : MonoBehaviour {

	public Transform player;
	public PlayerController player2;
	public GameObject grenade;
	public Vector3 targetPosition;
	public Vector3 prevPosition;
	public Vector3 throwGrenade;
	public Vector2 mousePosition;
	public Vector2 mousePosition1;
	public float mouseAngle;
    private int grenadeNum;
    public bool throwing = false;
	

	// Use this for initialization
	void Start () {
		grenadeNum = 3;		
	}
	
	// Update is called once per frame
	void Update () {
				
		if (throwing)
		{
            throwing = false;
			mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - player.transform.position;
			mouseAngle = Mathf.Atan2(mousePosition.y, mousePosition.x) * Mathf.Deg2Rad;

			mousePosition1 = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			targetPosition = new Vector3(mousePosition1.x, mousePosition1.y, player.transform.position.z);
			prevPosition = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
			if (targetPosition.x >= prevPosition.x)
			{
				throwGrenade = new Vector3(player.transform.position.x + 0.4f, player.transform.position.y + 0.15f, player.transform.position.z);
			}
			else
			{
				throwGrenade = new Vector3(player.transform.position.x - 0.4f, player.transform.position.y + 0.15f, player.transform.position.z);
			}

			Instantiate(grenade, throwGrenade, player.transform.rotation);
			grenadeNum--;


		}

		if (grenadeNum == 0)
		{
						
			//grenadeNum = 3;
		}

	}
}
