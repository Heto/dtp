﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anim : MonoBehaviour {

    private Animator anim;
    public PlayerController player;
    public bool canRoll;
    public GameObject gameOverUI;
    
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        canRoll = true;
    }
	
	// Update is called once per frame
	void Update () {
       if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            anim.SetBool("isMoving", true);
        }
        else
        {
            anim.SetBool("isMoving", false);
        }

       if (Input.GetMouseButtonDown(1) && canRoll && player.stamina > 0 && player.isGameOvered == false)
        {
            anim.SetTrigger("roll");
            canRoll = false;
        }

       if (player.isGameOvered)
        {
            anim.SetBool("isDead", true);
        }

    }

    void IsRollingEnd()
    {
        canRoll = true;
    }

    void IsDeadEnd()
    {
        gameOverUI.SetActive(true);
    }
}
