﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBoxPrice : MonoBehaviour {

    public WeaponBox weaponBox;
    public TextMesh text;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        text.text = weaponBox.price.ToString();
    }
}
