﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{

	public float getactiveMoveSpeed;
	private float timer;
	public float gettimer;
	private Vector2 target;
	private float MonsterScale;

    private float x, y;

	[HideInInspector]
	public float activeMoveSpeed;
	[HideInInspector]
	public bool isSturn;

	void Start()
	{
        x = transform.localScale.x;
        y = transform.localScale.y;
		isSturn = false;
		MonsterScale = Random.Range(-0.05f, 0.1f);
		target = new Vector2(10.0f, 10.0f);
		activeMoveSpeed = getactiveMoveSpeed;
		timer = gettimer;
	}

	public void GetTarget(Vector2 tar)
	{
		target = tar;
	}
	public int FindPlayer(PlayerController Player)
	{
		if (Player.transform.position.y > transform.position.y + 1f)
		{
			// Player is up floor than Monster
			return 2;
		}
		else if (Player.transform.position.y + 1f < transform.position.y)
		{
			//Player is down floor than Monster
			return 0;
		}
		else
		{
			//Player is same floor than Monster
			return 1;
		}
	}
	public float SetToTarget(int getpostype, PlayerController Player)
	{
		if (getpostype == 1)
		{
			target = Player.transform.position;
			return Vector2.Distance(target, transform.position);

		}
		else if (getpostype == 2)
		{
			target = new Vector2(3f, transform.position.y);
			return 100;
		}
		else if (getpostype == 0)
		{
			target = new Vector2(-4.9f, transform.position.y);
			return 100;
		}
		else
		{
			//not found target
			return 10000;
		}

	}
	public void flip(bool IsLeft)
	{
        if (IsLeft)
            transform.localScale = new Vector3(x + MonsterScale, y + MonsterScale, 1f);
        else
            transform.localScale = new Vector3(-(x + MonsterScale), (y + MonsterScale), 1f);
    }
	public void flipNormal(bool isleft)
	{
		if (isleft)
			transform.localScale = new Vector3(1f, 1f, 1f);
		else
			transform.localScale = new Vector3(-(1f), 1f, 1f);
	}

	public bool MoveToTarget()
	{

		if (target.x > transform.position.x)
		{
			//transform.localScale = new Vector3(1f, 1f, 1f);

			transform.position = Vector2.MoveTowards(transform.position, target, activeMoveSpeed * Time.deltaTime);
			return true;
		}
		else
		{
			//transform.localScale = new Vector3(-1f, 1f, 1f);
			transform.position = Vector2.MoveTowards(transform.position, target, activeMoveSpeed * Time.deltaTime);
			return false;
		}
	}
    public bool GetFlipCondition()
    {
        if (target.x > transform.position.x)
        {
            transform.position = Vector2.MoveTowards(transform.position, target, 0 * Time.deltaTime);
            return true;
        }
        else
        {
            transform.position = Vector2.MoveTowards(transform.position, target, 0 * Time.deltaTime);
            return false;
        }
    }
	public void GetStun()
	{
		activeMoveSpeed = 0f;
		isSturn = true;
	}
	public void CircularTimer()
	{
		if (activeMoveSpeed == 0f)
		{
			if (timer >= 0)
			{
				timer -= Time.deltaTime;
			}
			else
			{
				activeMoveSpeed = getactiveMoveSpeed;
				timer = gettimer;
				isSturn = false;
			}
		}
	}




}
