﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartridgeCase : MonoBehaviour {

    public Rigidbody2D rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        Throwing();
        transform.rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Throwing()
    {
        Vector3 sp = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 dir = -(Input.mousePosition - sp).normalized;
        rb.AddForce(dir * Random.Range(30f, 400f));
    }
}
