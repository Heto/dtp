﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie_Boss_ArmShoot : MonoBehaviour
{
    public BulletController bullet;
	public float destroytime;
    public float getdamage;

    public int type;
    //1 = zombie
    //2 = boss

    // Use this for initialization
    void Start()
	{
        
		Destroy(this.gameObject, destroytime);
	}

	// Update is called once per frame
	void Update()
	{
		//transform.Translate(Vector3.right * Time.deltaTime * speed);

	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Ground")
		{
			Destroy(gameObject);
		}
		if (col.gameObject.tag == "Bullet")
		{
            if (type == 1)
            {
                var colBullet = col.gameObject.AddComponent<BulletController>();
                if (colBullet.pierceNumber > 0)
                    colBullet.pierceNumber--;
                else
                    Destroy(colBullet.gameObject);
                Destroy(gameObject);
            }
		}
        if (col.tag == "Player")
        {

            col.GetComponent<PlayerController>().GetDamage(getdamage);

            Destroy(gameObject);
        }
    }

}