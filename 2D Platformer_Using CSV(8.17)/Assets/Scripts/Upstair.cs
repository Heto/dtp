﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upstair : MonoBehaviour
{

    [HideInInspector] public PlayerController player;
    public bool setActive;
    // public WaveSpawner waveSpawner;
    public WaveSpawner getWaveSpawner;
    public bool nextWave;
    public SpriteRenderer spr;
    // Use this for initialization
    void Start()
    {
        setActive = false;
        nextWave = false;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        getWaveSpawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<WaveSpawner>();
        spr = GetComponent<SpriteRenderer>();
        spr.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (setActive == true && nextWave == true)
        {
            waveSpawner.hatchOpened = true;
            nextWave = false;
        }*/

    }

    void OnTriggerStay2D(Collider2D col)
    {

        if (col.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (setActive == true)
                {
                    player.takeUpstair = true;

                }

            }

        }

    }
    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "hatch")
        {

            getWaveSpawner.GetComponent<WaveSpawner>().spawnMax += 2;
            player.oxygenpoint = 100;
            setActive = true;
        }
    }
}
