﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletUI : MonoBehaviour
{
    public Text Bullet;
    public Weapon weapon;
    public PlayerController player;

    // Use this for initialization
    void Start()
    {
        Bullet = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        weapon = WeaponSlot.GetWeapon();
        Bullet.text = weapon.BulletCount + "/" + weapon.staticBulletCount;
    }

}
