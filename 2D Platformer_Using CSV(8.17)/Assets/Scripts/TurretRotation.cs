﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretRotation : MonoBehaviour {

    public bool enemyInRange;
    public GameObject colenemy;

    void Start()
    {
        enemyInRange = false;
    }
    // Update is called once per frame
    void Update () {
		
	}

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            enemyInRange = true;
            colenemy = col.gameObject;
        }

        
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy")
            enemyInRange = false;

    }

}
