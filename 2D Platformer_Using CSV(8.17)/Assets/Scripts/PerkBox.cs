﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerkBox : MonoBehaviour {

    public PlayerController player;
    public PerksManager perks;
    public Animator anim;
    public GameObject perkText;
    public GetPerkText getPerkText;
    public int price;
    public int randomPerk;
    public Vector3 playerPos;
    public Vector3 myPos;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
        perks = GameObject.FindObjectOfType<PerksManager>();
        anim = GetComponent<Animator>();
        if (perks.perk_Bargain)
            price = price / 2;
        else
            price = 100;
    }

    // Update is called once per frame
    void Update()
    {
        playerPos = new Vector3(player.transform.position.x, player.transform.position.y - 1f, player.transform.position.z);
        myPos = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z - 0.2f);
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E) && player.energy >= price && anim.GetBool("canUse"))
            {
                player.energy -= price;
                if (perks.perk_Bargain)
                    price += price / 2;
                else
                    price += price;
                anim.SetTrigger("use");
            }
        }
    }

    void AnimStart()
    {
        anim.SetBool("canUse", false);
    }

    void AnimEnd()
    {
        anim.SetTrigger("toIdle");
        anim.SetBool("canUse", true);

        randomPerk = Random.Range(1, 29);

        switch(randomPerk)
        {
            case 1:
                if (perks.perk_Muscular)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Muscular = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 2:
                if (perks.perk_Engineer)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Engineer = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 3:
                if (perks.perk_Cannibalism)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Cannibalism = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 4:
                if (perks.perk_Asthma)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Asthma = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 5:
                if (perks.perk_ThirdArm)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_ThirdArm = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 6:
                if (perks.perk_TurretMaster)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_TurretMaster = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 7:
                if (perks.perk_UtilityBelt)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_UtilityBelt = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 8:
                if (perks.perk_Breacher)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Breacher = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 9:
                if (perks.perk_Hitman)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Hitman = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 10:
                if (perks.perk_Pointman)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Pointman = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 11:
                if (perks.perk_Rifleman)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Rifleman = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 12:
                if (perks.perk_Patience)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Patience = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 13:
                if (perks.perk_Bargain)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Bargain = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 14:
                if (perks.perk_Compensator)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Compensator = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 15:
                if (perks.perk_ExplosiveBullet)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_ExplosiveBullet = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 16:
                if (perks.perk_BulletSpeedUp)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_BulletSpeedUp = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 17:
                if (perks.perk_Adrenaline)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Adrenaline = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 18:
                if (perks.perk_Calm)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Calm = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 19:
                if (perks.perk_Berserker)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Berserker = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 20:
                if (perks.perk_Brave)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Brave = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 21:
                if (perks.perk_Ninja)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Ninja = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 22:
                if (perks.perk_SavingSpirit)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_SavingSpirit = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 23:
                if (perks.perk_Shout)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Shout = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 24:
                if (perks.perk_Rashness)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Rashness = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 25:
                if (perks.perk_Slayer)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Slayer = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 26:
                if (perks.perk_AddMagazine)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_AddMagazine = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 27:
                if (perks.perk_ArmorPierce)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_ArmorPierce = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
            case 28:
                if (perks.perk_Sniper)
                {
                    AnimEnd();
                    break;
                }
                else
                {
                    perks.perk_Sniper = true; Instantiate(perkText, myPos, Quaternion.identity); break;
                }
        }


    }
}
