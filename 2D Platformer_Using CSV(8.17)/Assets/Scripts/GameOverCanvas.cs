﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverCanvas : MonoBehaviour {
    public Transform player;
    public Vector3 offset;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(player.transform.position);
        offset = player.transform.position;

        transform.position = Vector3.Lerp(transform.position, offset, Time.deltaTime * 3f);
	}
}
