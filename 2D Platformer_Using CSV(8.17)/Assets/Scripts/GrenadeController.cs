﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeController : MonoBehaviour
{
	public ItemController player;
	public GameObject explosionEffect;
	//출발지
	public Vector3 vStartPos;
	//도착지
	public Vector3 vEndPos;
	//현재 위치
	public Vector3 vPos;

	public float speed = 4.0f;
	public float g = 9.8f;
	public float angle;
	float fTime;

	// Use this for initialization
	void Start()
	{
		if (GameObject.Find("Grenade"))
		{
			player = GameObject.Find("Grenade").GetComponent<ItemController>();
			vStartPos = player.prevPosition;
			vEndPos = player.targetPosition;
			angle = player.mouseAngle;
		}
		
	}

	// Update is called once per frame
	void Update()
	{
		Fire();
	}


	public void Fire()
	{
		fTime += 2.0f * Time.deltaTime;

		if (vEndPos.x >= vStartPos.x)
		{
			vPos.x = (vEndPos.x - vStartPos.x) + speed;
		}

		if (vEndPos.x < vStartPos.x)
		{
			vPos.x = (vEndPos.x - vStartPos.x) - speed;
		}

		vPos.y = vEndPos.y + 2.0f + Mathf.Sin(angle) * speed * fTime - (0.5f * g * fTime * fTime);

		Vector3 direction = vPos;
		GetComponent<Rigidbody2D>().velocity = direction;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Ground")
		{
			Instantiate(explosionEffect, transform.position + new Vector3(0, 0.5f, 0), transform.rotation);
			Destroy(gameObject);
		}

		if (col.gameObject.tag == "Enemy")
		{
			Instantiate(explosionEffect, transform.position, transform.rotation);
			Destroy(gameObject);

		}
	}
}




