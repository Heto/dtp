﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTrigger : MonoBehaviour
{
    public float dmg;
    public float barricadeDmg;
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
           
            col.GetComponent<PlayerController>().GetDamage(dmg);
            //col.GetComponent<PlayerController>().Knockback();
            Debug.Log("hit player");
        }
        if (col.gameObject.tag == "barricade")
        {
            col.SendMessage("ApplyDamage", barricadeDmg);
        }

    }

}
