﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerksManager : MonoBehaviour
{
	public PlayerController player;
	public Hatch[] hatch;
    public Weapon weapon;
    public GameObject pistol;
    public GameObject mp5;
    public GameObject ak;
    public GameObject doublebarrel;
    public GameObject pump;
    public GameObject kar98;
    public GameObject de;
    public GameObject ingram;
    public GameObject hk416;
    public GameObject mk14;
    public GameObject m249;
    public GameObject pulse;
    public bool perk_Muscular;
	public bool perk_Engineer;
	public bool perk_Cannibalism;
	public bool perk_Asthma;
	public bool perk_ThirdArm;
	public bool perk_TurretMaster;
	public bool perk_UtilityBelt;
	public bool perk_Breacher;
	public bool perk_Hitman;
	public bool perk_Pointman;
	//라이플 맨 o ->웨폰 스크립트
	public bool perk_Rifleman;
	//인내력 o
	public bool perk_Patience;
    //흥정 (나중에)
    public bool perk_Bargain;
    //보정기 (샷건 제외) o ->불렛 스크립트
    public bool perk_Compensator;
    //폭발성 화약 o ->웨폰 스크립트
    public bool perk_ExplosiveBullet;
    //트리플 베이스 화약o
    public bool perk_BulletSpeedUp;
    //아드레날린
    public bool perk_Adrenaline;
    //침착함 o ->웨폰 스크립트
    public bool perk_Calm;
    //광전사
    public bool perk_Berserker;
    //용기
    public bool perk_Brave;
    //닌자 o
    public bool perk_Ninja;
    private bool checkSpeedUp = true;
    //절약정신 o ->EnemyDie 스크립트
    public bool perk_SavingSpirit;
    //고함
    public bool perk_Shout;
    //무모함
    public bool perk_Rashness;
    //학살자
    public bool perk_Slayer;
    //탄창확장 o ->웨폰 스크립트
    public bool perk_AddMagazine;
    public bool perk_ArmorPierce;
    public bool perk_Sniper;

    // Use this for initialization
    void Start()
	{
		perk_Muscular = false;
		perk_Engineer = false;
		perk_Cannibalism = false;
		perk_Asthma = false;
		perk_ThirdArm = false;
		perk_TurretMaster = false;
		perk_UtilityBelt = false;
		perk_Breacher = false;
		perk_Hitman = false;
		perk_Pointman = false;
		perk_Rifleman = false;
		perk_Patience = false;
        perk_Bargain = false;
        perk_Compensator = false;
        perk_ExplosiveBullet = false;
        perk_BulletSpeedUp = false;
        perk_Adrenaline = false;
        perk_Calm = false;
        perk_Berserker = false;
        perk_Brave = false;
        perk_Ninja = false;
        perk_SavingSpirit = false;
        perk_Shout = false;
        perk_Rashness = false;
        perk_Slayer = false;
        perk_AddMagazine = false;
        perk_ArmorPierce = false;
        perk_Sniper = false;

        hatch = FindObjectsOfType<Hatch>();
    }

	// Update is called once per frame
	void Update()
	{
        weapon = WeaponSlot.GetWeapon();

        if (perk_Muscular == true)
            player.hatchOpenSpeed = 1f;

        if (perk_Engineer == true)
		{
            player.barricadeFixSpeed = 10f;
		}

		if (perk_Asthma == true)
		{
			player.oxygenpoint -= Time.deltaTime * 0.5f;
		}

        //인내력
        if (perk_Patience == true)
        {
            player.staticHp = player.staticHp * 1.2f;
            perk_Patience = false;
        }

        //닌자
        if (perk_Ninja == true && checkSpeedUp)
        {
            player.moveSpeed = player.moveSpeed * 1.2f;
            checkSpeedUp = false;
        }

    }

	public void ActivateCannibalism()
	{
		if (perk_Cannibalism == true)
		{
			player.hp += 1;
		}
	}
}
