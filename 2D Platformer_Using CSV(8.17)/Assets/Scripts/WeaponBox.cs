﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBox : MonoBehaviour {

    public GameObject mp5;
    public GameObject shotgun;
    public GameObject grenade;
	public GameObject ak;
	public GameObject kar98;
	public GameObject pump;
    public GameObject de;
    public GameObject ingram;
    public GameObject hk416;
    public GameObject mk14;
    public GameObject m249;
    public GameObject pulse;

    public PlayerController player;
    public PerksManager perks;
    private Vector3 spawnPosition;

    public Animator anim;

    public int randomGun;//1 mp5 2 shotgun 3 grenade
    public int price;

	// Use this for initialization
	void Start () {
        player = GameObject.FindObjectOfType<PlayerController>();
        perks = GameObject.FindObjectOfType<PerksManager>();
        spawnPosition = new Vector3(transform.position.x, transform.position.y, -0.1f);
        anim = GetComponent<Animator>();
        if (perks.perk_Bargain)
            price = price / 2;
        else
            price = 100;
    }
	
	// Update is called once per frame
	void Update () {
                    
	}

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E) && player.energy >= price && anim.GetBool("canUse"))
            {
                player.energy -= price;
                if (perks.perk_Bargain)
                    price += price / 2;
                else
                    price += price;
                anim.SetTrigger("use");
                
                //Destroy(gameObject);
            }

        }

    }

    void AnimStart()
    {
        anim.SetBool("canUse", false);
    }

    void AnimEnd()
    {
        anim.SetTrigger("toIdle");
        anim.SetBool("canUse", true);
        randomGun = Random.Range(1, 12);

        if (randomGun == 1)
        {
            if (WeaponSlot.slotList.Contains(player.mp5))
            {
                AnimEnd();
                return;
            }                
            else
            {
                var clone = Instantiate(mp5, spawnPosition, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
            }
        }
        if (randomGun == 2)
        {
            if (WeaponSlot.slotList.Contains(player.shotgun))
            {
                AnimEnd();
                return;
            }                
            else
            {
                var clone = Instantiate(shotgun, spawnPosition, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
            }
        }
        if (randomGun == 3)
        {
            if (WeaponSlot.slotList.Contains(player.ak))
            {
                AnimEnd();
                return;
            }            
            else
            {
                var clone = Instantiate(ak, spawnPosition, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
            }
        }
        if (randomGun == 4)
        {
            if (WeaponSlot.slotList.Contains(player.kar98))
            {
                AnimEnd();
                return;
            }                
            else
            {
                var clone = Instantiate(kar98, spawnPosition, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
            }
        }
        if (randomGun == 5)
        {
            if (WeaponSlot.slotList.Contains(player.pump))
            {
                AnimEnd();
                return;
            }                
            else
            {
                var clone = Instantiate(pump, spawnPosition, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
            }
        }
        if (randomGun == 6)
        {
            if (WeaponSlot.slotList.Contains(player.de))
            {
                AnimEnd();
                return;
            }                
            else
            {
                var clone = Instantiate(de, spawnPosition, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
            }
        }
        if (randomGun == 7)
        {
            if (WeaponSlot.slotList.Contains(player.ingram))
            {
                AnimEnd();
                return;
            }                
            else
            {
                var clone = Instantiate(ingram, spawnPosition, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
            }
        }
        if (randomGun == 8)
        {
            if (WeaponSlot.slotList.Contains(player.hk416))
            {
                AnimEnd();
                return;
            }                
            else
            {
                var clone = Instantiate(hk416, spawnPosition, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
            }
        }
        if (randomGun == 9)
        {
            if (WeaponSlot.slotList.Contains(player.mk14))
            {
                AnimEnd();
                return;
            }                
            else
            {
                var clone = Instantiate(mk14, spawnPosition, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
            }
        }
        if (randomGun == 10)
        {
            if (WeaponSlot.slotList.Contains(player.m249))
            {
                AnimEnd();
                return;
            }                
            else
            {
                var clone = Instantiate(m249, spawnPosition, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
            }
        }
        if (randomGun == 11)
        {
            if (WeaponSlot.slotList.Contains(player.pulse))
            {
                AnimEnd();
                return;
            }                
            else
            {
                var clone = Instantiate(pulse, spawnPosition, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
            }
        }
    }
}
