﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public PerksManager perks;
    public Weapon weapon;
	public int pierceNumber;
    private float spd;

    // Use this for initialization
    void Start()
    {
        perks = GameObject.FindObjectOfType<PerksManager>();
        weapon = WeaponSlot.GetWeapon();
        spd = weapon.bulletSpeed * 1.2f;
        if (WeaponSlot.GetWeaponIdx() == 4)
            if (perks.perk_Sniper)
                pierceNumber = 4;
            else
                pierceNumber = 2;
        else if (WeaponSlot.GetWeaponIdx() == 9)
            if (perks.perk_Sniper)
                pierceNumber = 5;
            else
                pierceNumber = 4;
        else if (WeaponSlot.GetWeaponIdx() == 2)
        {
            var per = Random.Range(0, 2);
            if (per == 0)
                pierceNumber = 1;
        }
        else if (WeaponSlot.GetWeaponIdx() == 5)
        {
            var per = Random.Range(0, 2);
            if (per == 0)
                pierceNumber = 1;
        }

        else
            pierceNumber = 0;

        if (perks.perk_ArmorPierce)
            pierceNumber += 1;

        Destroy(gameObject, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        if (perks.perk_BulletSpeedUp)
            transform.Translate(Vector3.right * Time.deltaTime * spd);
        else
            transform.Translate(Vector3.right * Time.deltaTime * weapon.bulletSpeed);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Ground")
        {
            if (weapon.perks.perk_Compensator == true && WeaponSlot.GetWeaponIdx() != 11)
            {
                int ran = Random.Range(0, 2);
                if (ran == 0)
                    weapon.BulletCount++;
            }

            if (WeaponSlot.GetWeaponIdx() != 11)
                Destroy(gameObject);
        }
        if (col.gameObject.tag == "Enemy")
        {
			if (pierceNumber > 0)
			{
				pierceNumber--;
			}
			else
				Destroy(gameObject);
			
        }

    }

    //void OnTriggerStay2D(Collider2D col)
    //{
    //    if (col.gameObject.tag == "Ground")
    //    {
    //        if (weapon.perks.perk_Compensator == true)
    //        {
    //            weapon.BulletCount++;
    //        }
    //        Destroy(gameObject);
    //    }
        
    //}
}
