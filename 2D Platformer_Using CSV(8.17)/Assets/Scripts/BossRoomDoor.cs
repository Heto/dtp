﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRoomDoor : MonoBehaviour
{
    public GameObject block;
    public GameObject bossAlive;
    public GameObject door;

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                
                if (bossAlive.GetComponent<Animator>().GetBool("die") == true)
                {
                    Destroy(block);
                    Destroy(door);
                }
            }
        }

    }
}
