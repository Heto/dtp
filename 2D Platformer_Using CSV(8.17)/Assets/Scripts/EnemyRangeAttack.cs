﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangeAttack : MonoBehaviour
{
	public GameObject EnemyBullet; // bullet
	public Transform firePoint; // bullet shoot position
    /*
	private float shootdelay; // shoot first delay time
	private float shootCd; // shoot cool down



	public float getdelay; // get shoot first delay time
	public float getCd; // get shoot cool down
	public float condition; // shoot 조건
							// Use this for initialization
	[HideInInspector]
	public bool ShootStart;
	public bool conditionOK;
	public bool CdOK;
    */
	void Start()
	{
        /*
		shootdelay = getdelay;
		shootCd = getCd;

		ShootStart = false;
		conditionOK = false;
		CdOK = false;
        */
	}

    public void ShootAnimStart(Animator anim)
    {
        anim.SetBool("shoot", true);
        anim.SetBool("move", false);

    }
    public void ShootAnimEnd(Animator anim)
    {
        anim.SetBool("shoot", false);
        anim.SetBool("move", true);
    }

    public void ShootBullet(bool IsLeft)
    {
        GameObject bullet = (GameObject)Instantiate(EnemyBullet, firePoint.position, firePoint.rotation);
        if (IsLeft)
        {
            bullet.GetComponent<Rigidbody2D>().velocity = Vector2.right * 13;

        }
        else
        {
            bullet.GetComponent<Rigidbody2D>().velocity = Vector2.left * 13;

        }
    }
    /*

	public void ShootBullet(bool IsLeft)
	{

		if (shootdelay > 0)
		{
			shootdelay -= Time.deltaTime;
		}
		else
		{
			GameObject bullet = (GameObject)Instantiate(EnemyBullet, firePoint.position, firePoint.rotation);
			if (IsLeft)
			{
				bullet.GetComponent<Rigidbody2D>().velocity = Vector2.right * 10;

			}
			else
			{
				bullet.GetComponent<Rigidbody2D>().velocity = Vector2.left * 10;

			}
			shootCd = getCd;
			shootdelay = getdelay;
			ShootStart = false;
			conditionOK = false;
		}


	}
    
	public void CheckingAttack(float distance)
	{
		CheckCondition(distance);
		CheckCd();
		if (CdOK && conditionOK)
		{

			ShootStart = true;

		}

	}
	public void CheckCondition(float distance)
	{
		if (condition > distance)
		{
			conditionOK = true;
		}
	}
	public void CheckCd()
	{
		if (shootCd >= 0)
		{
			CdOK = false;
			shootCd -= Time.deltaTime;
		}
		else
		{
			CdOK = true;
		}
	}
    */
}
