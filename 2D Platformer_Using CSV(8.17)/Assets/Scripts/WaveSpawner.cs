﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{

    public int remainEnemy;
    private int spawnPointSelect;
    public int enemySelect;
    public Transform spawnPosition1;
    public Transform spawnPosition2;
    public Transform spawnPosition3;
    public Transform spawnPosition4;
    public GameObject enemy_normal;
    public GameObject enemy_fast;
    public GameObject enemy_range;
    public GameObject enemy_tanker;
    public bool hatchOpened;

    public GameObject getMapGemerator;
    private List<Transform> spawnPointList;

    [HideInInspector] public int spawnMax;
    // 0,1 = 1층스폰
    // 2,3 = 2층스폰
    // 4,5 = 3층스폰
    // ....
    public bool spawnCheck;
    //스폰체크 , 껐다켰다.

    // Use this for initialization
    void Start()
    {
        spawnMax = 1;
        spawnCheck = true;
        spawnPointList = null;
        hatchOpened = false;
        // StartCoroutine(Wave1());
        StartCoroutine(EnemyWave());
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnCheck == false)
            StopCoroutine(EnemyWave());        
        //if (hatchOpened == true)
        //{
        //    StartCoroutine(Wave2());
        //    hatchOpened = false;
        //}
    }
    public void EndEnemy()
    {

        EnemyController closetEnemy = null;
        EnemyController[] allEnemies = GameObject.FindObjectsOfType<EnemyController>();

        foreach(EnemyController currentEnemy in allEnemies)
        {
            currentEnemy.hp -= 500;
        }
    }

    IEnumerator Wave1()
    {
        remainEnemy = 200;


        while (remainEnemy > 0)
        {
            spawnPointSelect = Random.Range(1, 3);

            if (spawnPointSelect == 1)
            {
                enemySelect = Random.Range(1, 5);

                if (enemySelect == 1)
                    Instantiate(enemy_normal, spawnPosition1.transform.position, transform.rotation);
                if (enemySelect == 2)
                    Instantiate(enemy_fast, spawnPosition1.transform.position, transform.rotation);
                if (enemySelect == 3)
                    Instantiate(enemy_range, spawnPosition1.transform.position, transform.rotation);
                if (enemySelect == 4)
                    Instantiate(enemy_tanker, spawnPosition1.transform.position, transform.rotation);
            }

            if (spawnPointSelect == 2)
            {
                enemySelect = Random.Range(1, 5);

                if (enemySelect == 1)
                    Instantiate(enemy_normal, spawnPosition2.transform.position, transform.rotation);
                if (enemySelect == 2)
                    Instantiate(enemy_fast, spawnPosition2.transform.position, transform.rotation);
                if (enemySelect == 3)
                    Instantiate(enemy_range, spawnPosition2.transform.position, transform.rotation);
                if (enemySelect == 4)
                    Instantiate(enemy_tanker, spawnPosition2.transform.position, transform.rotation);
            }

            remainEnemy--;

            yield return new WaitForSeconds(1.0f);
        }
    }

    IEnumerator Wave2()
    {
        remainEnemy = 200;


        while (remainEnemy > 0)
        {
            spawnPointSelect = Random.Range(1, 3);

            if (spawnPointSelect == 1)
            {
                enemySelect = Random.Range(1, 5);

                if (enemySelect == 1)
                    Instantiate(enemy_normal, spawnPosition3.transform.position, transform.rotation);
                if (enemySelect == 2)
                    Instantiate(enemy_fast, spawnPosition3.transform.position, transform.rotation);
                if (enemySelect == 3)
                    Instantiate(enemy_range, spawnPosition3.transform.position, transform.rotation);
                if (enemySelect == 4)
                    Instantiate(enemy_tanker, spawnPosition3.transform.position, transform.rotation);
            }

            if (spawnPointSelect == 2)
            {
                enemySelect = Random.Range(1, 5);

                if (enemySelect == 1)
                    Instantiate(enemy_normal, spawnPosition4.transform.position, transform.rotation);
                if (enemySelect == 2)
                    Instantiate(enemy_fast, spawnPosition4.transform.position, transform.rotation);
                if (enemySelect == 3)
                    Instantiate(enemy_range, spawnPosition4.transform.position, transform.rotation);
                if (enemySelect == 4)
                    Instantiate(enemy_tanker, spawnPosition4.transform.position, transform.rotation);
            }

            remainEnemy--;

            yield return new WaitForSeconds(2.5f);
        }
    }

    IEnumerator EnemyWave()
    {
        yield return new WaitForSeconds(2.0f);
        if (spawnPointList == null)
        {
            spawnPointList = getMapGemerator.GetComponent<MapGenerator>().getSpawnPointList();
        }
        while (spawnCheck)
        {
            for (int i = 0; i < spawnMax + 1; i++)
            {
                SpawnEnemy(spawnPointList[i],i);
            }
            yield return new WaitForSeconds(6.0f);
        }


    }
    void SpawnEnemy(Transform pos,int num)
    {
        //num = 0,1 1층
        //num = 2,3 2층
        //num = 4,5 3층
        //num = 6,7 4층
        //num = 8,9 5층
        //num = 10,11 6층
        //num = 12,13 7층
        int spawnfloor = (int)((num / 10) + 1);
        float randnum = Random.Range(0.0f, 1.0f);
        if (randnum <= 0.4f)
        {
            GameObject temp = Instantiate(enemy_normal, pos.transform.position, transform.rotation);
            temp.GetComponent<EnemyController>().hp += spawnfloor * 8;
            temp.GetComponent<EnemyMove>().activeMoveSpeed += spawnfloor * 0.03f;
        }
        else if (0.4f < randnum && randnum <= 0.75f)
        {
            GameObject temp = Instantiate(enemy_fast, pos.transform.position, transform.rotation);
            temp.GetComponent<EnemyController>().hp += spawnfloor  * 3;
            temp.GetComponent<EnemyMove>().activeMoveSpeed += spawnfloor * 0.07f;
        }
        else if (0.75f < randnum && randnum <= 0.85f)
        {
            GameObject temp = Instantiate(enemy_tanker, pos.transform.position, transform.rotation);
            temp.GetComponent<EnemyController>().hp += spawnfloor * 10;
            temp.GetComponent<EnemyMove>().activeMoveSpeed += spawnfloor * 0.01f;
        }
        else if (0.85f < randnum && randnum <= 1.0f)
        {
            GameObject temp = Instantiate(enemy_range, pos.transform.position, transform.rotation);
            temp.GetComponent<EnemyController>().hp += spawnfloor * 5;
            temp.GetComponent<EnemyMove>().activeMoveSpeed += spawnfloor * 0.01f;
        }
    }
}
