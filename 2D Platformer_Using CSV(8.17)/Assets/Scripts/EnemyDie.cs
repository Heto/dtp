﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDie : MonoBehaviour
{
	public Collider2D boomTrigger;
	public PerksManager perks;
	public PlayerController player;
    public Weapon weapon;
    public bool enemyDieCheck = false;
    public int randomSavingSpirit;
    public int EnemyType;

    [HideInInspector]public bool isDying = false;

	void Start()
	{
		player = GameObject.Find("Player").GetComponent<PlayerController>();
		perks = GameObject.Find("Player").GetComponent<PerksManager>();
        weapon = WeaponSlot.GetWeapon();
    }
    public void BoomerAttackStart(float hp, BoxCollider2D[] a)
    {
        isDying = true;
        
        for (int i = 0; i < a.Length; i++)
        {
            a[i].enabled = false;
        }
        this.GetComponent<Rigidbody2D>().gravityScale = 0f;
        this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
    }
	public void DieCheck(float hp, Animator myAnim, BoxCollider2D[] a)
	{
		if (EnemyType != 4)
			boomTrigger = null;
        if (hp <= 0)
        {
            if (EnemyType == 4)
            {
                myAnim.SetBool("attack", true);
                isDying = true;
                for (int i = 0; i < a.Length; i++)
                {
                    a[i].enabled = false;
                }
                this.GetComponent<Rigidbody2D>().gravityScale = 0f;
                this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            }
            //animation 있는 좀비용 죽음
            else
            {
                myAnim.SetBool("die", true);                
                isDying = true;
                for (int i = 0; i < a.Length; i++)
                {
                    a[i].enabled = false;
                }
                this.GetComponent<Rigidbody2D>().gravityScale = 0f;
                this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            }

            //animation 없는 좀비용 죽음
           // Destroy(gameObject);

            StopAllCoroutines();
            //enemyDieCheck = true;

        }

        if (enemyDieCheck)
        {
            //IsEnemyDied();
            //player.accumulate_num++;
            //Debug.Log(player.accumulate_num);

            //if (perks.perk_SavingSpirit)
            //{
            //    randomSavingSpirit = Random.Range(1, 3);
            //    if (randomSavingSpirit == 2)
            //    {
            //        weapon.BulletCount++;
            //    }
            //}

            ////광전사 on
            //if (perks.perk_Berserker)
            //{
            //    weapon.perk_Berserker_Check = true;
            //}

            ////학살자 on
            //if (perks.perk_Slayer)
            //{
            //    weapon.perk_Slayer_Check = true;
            //}

            //if (perks.perk_Adrenaline)
            //{
            //    player.adrenalineOn = true;
            //}

            //enemyDieCheck = false;
        }
        
    }
	public void BoomTriggerOn()
	{
        boomTrigger.enabled = true;
	}
    public void BoomTriggerOff()
    {
        boomTrigger.enabled = false;
    }

    void IsEnemyDied()
    {
        player.energy += 10 + player.amountObtainEnergy;
        player.accumulate_num++;
        perks.ActivateCannibalism();
        if (perks.perk_SavingSpirit)
        {
            randomSavingSpirit = Random.Range(1, 3);
            if (randomSavingSpirit == 2)
            {
                weapon.BulletCount++;
            }
        }

        //광전사 on
        if (perks.perk_Berserker)
        {
            weapon.perk_Berserker_Check = true;
        }

        //학살자 on
        if (perks.perk_Slayer)
        {
            weapon.perk_Slayer_Check = true;
        }

        if (perks.perk_Adrenaline)
        {
            player.adrenalineOn = true;
        }
    }
}

