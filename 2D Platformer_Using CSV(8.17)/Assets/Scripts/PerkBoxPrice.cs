﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerkBoxPrice : MonoBehaviour {

    public PerkBox perkBox;
    public TextMesh text;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        text.text = perkBox.price.ToString();
    }
}
