﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public int idx;
    public GameObject slotUI;

	public bool isFiring;
	public GameObject bullet;
    public GameObject bullet_explosive;
    private float randomExplosive;
    public GameObject thirdArmBullet;
	public int BulletCount;
	public int staticBulletCount;
    public EnemyController enemy;

    public float bulletSpeed;

    public CsvInfo weaponInfo;

	public float timeBetweenShots;
	private float shotCounter;
	public float reloadtime;
	public float reloadStart;

	public bool damageInfo = true;
	public float minRecoil;
	public float maxRecoil;
	public float nowMinRecoil;
	public float nowMaxRecoil;
	public float recoilAmount;
	public float restoreRecoilAmount;
	public float staticDamage;
	public float damage;
	private int thirdArmProbability;

	//퍽 광전사 전용 bool변수
	public bool perk_Berserker_Check = false;
	//퍽 학살자 전용 bool변수
	public bool perk_Slayer_Check = false;

	public bool reloadcheck;
    public AudioClip reloadsound;

	public Transform firePoint;
	public Transform thirdArmPoint;
    private int thirdArmPercent;
    GameObject tempThirdBullet;
    GameObject shotgunBullet;
    GameObject tempMyBullet;
	GameObject tempMyThirdArmBullet;
    public GameObject shout;
    public Transform shoutPosition;

	public PlayerController player;
	public PerksManager perks;

    //
    public List<GameObject> Muzzel;
    public Transform cartPoint;
    public GameObject cart_pistol;
    public GameObject cart_rifle;
    public GameObject cart_shotgun;
    public GameObject cart_sniper;


    public AudioClip shotSound;
    private AudioSource source;
    private float volLowRange =0.5f;
    private float volHighRange = 1.0f;

	void Start()
	{
        if (shotSound != null)
        {
            source = GetComponent<AudioSource>();
        }
		perks = GameObject.Find("Player").GetComponent<PerksManager>();
		staticBulletCount = BulletCount;
		reloadcheck = false;

        weaponInfo = GetComponent<CsvInfo>();
        Debug.Log(WeaponSlot.GetWeaponIdx());
    }

    

    // Update is called once per frame
    void Update()
	{
		
        if (perks.perk_AddMagazine == true)
        {
            switch (WeaponSlot.GetWeaponIdx())
            {
                case 0:
                    staticBulletCount = weaponInfo.round + 5;
                    break;

                case 1:
                    staticBulletCount = weaponInfo.round + 10;
                    break;

                case 2:
                    staticBulletCount = weaponInfo.round + 2;
                    break;

                case 3:
					staticBulletCount = weaponInfo.round + 10;
					if (perks.perk_Rifleman)
					{
						staticBulletCount = weaponInfo.round + 20;
					}
					break;

				case 4:
					staticBulletCount = weaponInfo.round + 2;
					break;

				case 5:
					staticBulletCount = weaponInfo.round + 2;
					break;

                case 6:
                    staticBulletCount = weaponInfo.round + 2;
                    break;

                case 7:
                    staticBulletCount = weaponInfo.round + 10;
                    break;

                case 8:
                    staticBulletCount = weaponInfo.round + 10;
                    if (perks.perk_Rifleman)
                    {
                        staticBulletCount = weaponInfo.round + 20;
                    }
                    break;

                case 9:
                    staticBulletCount = weaponInfo.round + 2;
                    break;

                case 10:
                    staticBulletCount = weaponInfo.round + 20;
                    break;

                case 11:
                    staticBulletCount = weaponInfo.round + 3;
                    break;

                default:
                    break;
            }
        }
        else
        {
            staticBulletCount = weaponInfo.round;
			
		}
		timeBetweenShots = weaponInfo.rate;
		reloadtime = weaponInfo.reloadTime;
		recoilAmount = weaponInfo.recoil;		
		maxRecoil = weaponInfo.maxRecoil;
		minRecoil = weaponInfo.minRecoil;				
		restoreRecoilAmount = weaponInfo.restoreRecoil;

        if (perks.perk_Pointman && WeaponSlot.GetWeaponIdx() == 1)
        {
            maxRecoil = weaponInfo.maxRecoil / 2;
            minRecoil = weaponInfo.minRecoil / 2;
        }
        else if (perks.perk_Pointman && WeaponSlot.GetWeaponIdx() == 7)
        {
            maxRecoil = weaponInfo.maxRecoil / 2;
            minRecoil = weaponInfo.minRecoil / 2;
        }

        if (perks.perk_Rifleman && WeaponSlot.GetWeaponIdx() == 3)
        {
            staticBulletCount = weaponInfo.round + 20;
        }
        else if (perks.perk_Rifleman && WeaponSlot.GetWeaponIdx() == 8)
        {
            staticBulletCount = weaponInfo.round + 20;
        }

        if (damageInfo)
		{
			staticDamage = weaponInfo.damage;
			damage = staticDamage;
			damageInfo = false;
		}

		//퍽 광전사 on
		if (perk_Berserker_Check && (player.accumulate_num <= 30))
		{
			damage = staticDamage + staticDamage * player.accumulate_num * 0.02f;
            Debug.Log(damage);
			perk_Berserker_Check = false;
		}

		//퍽 무모함 on
		if (perks.perk_Rashness)
		{
			if (player.perk_Rashness_oxygenpointDiscount > 5.0f)
			{
				damage += 1.0f;
                Debug.Log(damage);
				player.perk_Rashness_oxygenpointDiscount = 0.0f;
			}

            if (player.hatchOpened)
            {
                damage = staticDamage;
                player.hatchOpened = false;
            }
		}
        
		shotCounter -= Time.deltaTime;

        if (Input.GetMouseButtonDown(0) && player.isGameOvered == false)
		{
			if (shotCounter <= 0)
				isFiring = true;
            if (BulletCount == 0)
            {
                source.PlayOneShot(reloadsound, .5f);
                reloadcheck = true;
                isFiring = false;
            }
		}
			

		if (Input.GetMouseButtonUp(0))
			isFiring = false;


		if (Input.GetKeyDown(KeyCode.R))
		{
            if (BulletCount != staticBulletCount)
            {
                isFiring = false;
                reloadcheck = true;
                source.PlayOneShot(reloadsound, .5f);             

                if (perks.perk_Shout)
                    Instantiate(shout, shoutPosition);
            }			
		}

		if (reloadcheck)
		{
			reloadStart += Time.deltaTime;

			if (isFiring == true)
				if (WeaponSlot.GetWeaponIdx() == 2 || WeaponSlot.GetWeaponIdx() == 5)
				{
                    source.clip = null;
                    source.loop = false;
                    reloadcheck = false;
					reloadStart = 0;
				}

		}

		//reloadtime만큼 시간이 지나면 장전
		if (reloadtime <= reloadStart && reloadcheck)
		{
			if (WeaponSlot.GetWeaponIdx() == 2 || WeaponSlot.GetWeaponIdx() == 5)
			{
				BulletCount++;
				reloadStart = 0;
                if (BulletCount == staticBulletCount)
                {
                    source.clip = null;
                    source.loop = false;
                    reloadcheck = false;                    
                }
                else
                {
                    source.PlayOneShot(reloadsound, .5f);
                    reloadcheck = true;
                }
                    
								
			}
			else
			{
                source.clip = null;
                source.loop = false;
                BulletCount = staticBulletCount;
				reloadStart = 0;
				reloadcheck = false;
			}			
		}

        //남은 탄 수가 탄창 용량보다 클 경우
        if (BulletCount > staticBulletCount)
        {
            BulletCount = staticBulletCount;
        }

        //침착성 on
        if (perks.perk_Calm && (player.moveCheck == false))
        {
            minRecoil = minRecoil * 0.5f;
            maxRecoil = maxRecoil * 0.5f;
        }

            //pistol
            if (isFiring && BulletCount > 0 && !reloadcheck && WeaponSlot.GetWeaponIdx() == 0)

            {

                if (shotCounter <= 0)
                {
                    //폭발성 화약 on
                    if (perks.perk_ExplosiveBullet == true)
                    {
                        randomExplosive = Random.Range(1, 6);
                        if (randomExplosive == 1)
                        {
                            shotCounter = timeBetweenShots;
                            tempMyBullet = Instantiate(bullet_explosive, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                        else if (!(randomExplosive == 1))
                        {
                            shotCounter = timeBetweenShots;
                            tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                    }

                    //폭발성 화약 off
                    else
                    {
                        shotCounter = timeBetweenShots;
                        tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                        Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                        tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        BulletCount--;
                        nowMinRecoil -= recoilAmount;
                        nowMaxRecoil += recoilAmount;

                        if (nowMinRecoil < minRecoil)
                            nowMinRecoil = minRecoil;
                        if (nowMaxRecoil > maxRecoil)
                            nowMaxRecoil = maxRecoil;
                    }

                    if (perks.perk_ThirdArm)
                    {
                        thirdArmPercent = Random.Range(1, 4);
                        if (thirdArmPercent == 1)
                        {
                            tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                            Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                            tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        }

                }
                source.PlayOneShot(shotSound, 1);
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
                GameObject tmpCart = Instantiate(cart_pistol, cartPoint.position, Quaternion.Euler(0, 0, 0));
                Destroy(tmpCart, 1f);
            }
                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }

            }

            //mp5
            else if (isFiring && BulletCount > 0 && !reloadcheck && WeaponSlot.GetWeaponIdx() == 1)

            {

            if (shotCounter <= 0)
            {
                if (perks.perk_Asthma == true)
                {
                    shotCounter = timeBetweenShots - 0.02f;
                }
                else
                {
                    shotCounter = timeBetweenShots;
                }

                //폭발성 화약 on
                if (perks.perk_ExplosiveBullet == true)
                {
                    randomExplosive = Random.Range(1, 6);
                    if (randomExplosive == 1)
                    {
                        tempMyBullet = Instantiate(bullet_explosive, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                        Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                        tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        BulletCount--;
                        nowMinRecoil -= recoilAmount;
                        nowMaxRecoil += recoilAmount;

                        if (nowMinRecoil < minRecoil)
                            nowMinRecoil = minRecoil;
                        if (nowMaxRecoil > maxRecoil)
                            nowMaxRecoil = maxRecoil;
                    }
                    else if (!(randomExplosive == 1))
                    {
                        tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                        Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                        tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        BulletCount--;
                        nowMinRecoil -= recoilAmount;
                        nowMaxRecoil += recoilAmount;

                        if (nowMinRecoil < minRecoil)
                            nowMinRecoil = minRecoil;
                        if (nowMaxRecoil > maxRecoil)
                            nowMaxRecoil = maxRecoil;
                    }
                }

                //폭발성 화약 off
                else
                {
                    tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                    Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                    tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                    BulletCount--;
                    nowMinRecoil -= recoilAmount;
                    nowMaxRecoil += recoilAmount;

                    if (nowMinRecoil < minRecoil)
                        nowMinRecoil = minRecoil;
                    if (nowMaxRecoil > maxRecoil)
                        nowMaxRecoil = maxRecoil;
                }

                if (perks.perk_ThirdArm)
                {
                    thirdArmPercent = Random.Range(1, 4);
                    if (thirdArmPercent == 1)
                    {
                        tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                        Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                        tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                    }
                }
                source.PlayOneShot(shotSound, 1);
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
                GameObject tmpCart = Instantiate(cart_pistol, cartPoint.position, Quaternion.Euler(0, 0, 0));
                Destroy(tmpCart, 1f);
            }

                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }
            }

            //doubleBarrel
            else if (isFiring && BulletCount > 0 && WeaponSlot.GetWeaponIdx() == 2)

            {

                if (shotCounter <= 0)
                {
                    shotCounter = timeBetweenShots;

                    int shotgunPellet = 6;

                    //폭발성 화약 on
                    if (perks.perk_ExplosiveBullet == true)
                    {
                        while (shotgunPellet > 0)
                        {
                            randomExplosive = Random.Range(1, 6);
                            Vector3 sp = Camera.main.WorldToScreenPoint(transform.position);
                            Vector3 dir = (Input.mousePosition - sp).normalized;
                            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                            float spread;
                            if (perks.perk_Breacher)
                                spread = Random.Range(-3, 3);
                            else
                                spread = Random.Range(-6, 6);
                            Quaternion bulletRotation = Quaternion.Euler(new Vector3(0, 0, angle + spread));
                            if (randomExplosive == 1)
                            {
                                shotgunBullet = (GameObject)GameObject.Instantiate(bullet_explosive, firePoint.position, bulletRotation);
                            }
                            else if (!(randomExplosive == 1))
                            {
                                shotgunBullet = (GameObject)GameObject.Instantiate(bullet, firePoint.position, bulletRotation);
                            }
                            Vector3 velocity = shotgunBullet.transform.rotation * Vector3.right;
                            shotgunBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            shotgunPellet--;
                        }

                    }

                    //폭발성 화약 off
                    else
                    {
                        // Instantiate the bullet using our new rotation
                        while (shotgunPellet > 0)
                        {
                            //폭발성 화약 off
                            Vector3 sp = Camera.main.WorldToScreenPoint(transform.position);
                            Vector3 dir = (Input.mousePosition - sp).normalized;
                            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                            float spread;
                            if (perks.perk_Breacher)
                                spread = Random.Range(-3, 3);
                            else
                                spread = Random.Range(-6, 6);
                            Quaternion bulletRotation = Quaternion.Euler(new Vector3(0, 0, angle + spread));
                            GameObject shotgunBullet = (GameObject)GameObject.Instantiate(bullet, firePoint.position, bulletRotation);
                            Vector3 velocity = shotgunBullet.transform.rotation * Vector3.right;
                            shotgunBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            shotgunPellet--;
                        }

                    }

                    if (perks.perk_ThirdArm)
                    {
                        thirdArmPercent = Random.Range(1, 4);
                        if (thirdArmPercent == 1)
                        {
                            tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                            Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                            tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        }
                    }

                    BulletCount--;
                    nowMinRecoil -= recoilAmount;
                    nowMaxRecoil += recoilAmount;

                    if (nowMinRecoil < minRecoil)
                        nowMinRecoil = minRecoil;
                    if (nowMaxRecoil > maxRecoil)
                        nowMaxRecoil = maxRecoil;
                source.PlayOneShot(shotSound, 1);
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
                GameObject tmpCart = Instantiate(cart_shotgun, cartPoint.position, Quaternion.Euler(0, 0, 0));
                Destroy(tmpCart, 1f);
            }

                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }
            }

            //ak
            else if (isFiring && BulletCount > 0 && !reloadcheck && WeaponSlot.GetWeaponIdx() == 3)

            {

                if (shotCounter <= 0)
                {
                    if (perks.perk_Asthma == true)
                    {
                        shotCounter = timeBetweenShots - 0.02f;
                    }
                    else
                    {
                        shotCounter = timeBetweenShots;
                    }

                    //폭발성 화약 on
                    if (perks.perk_ExplosiveBullet == true)
                    {
                        randomExplosive = Random.Range(1, 6);
                        if (randomExplosive == 1)
                        {
                            tempMyBullet = Instantiate(bullet_explosive, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                        else if (!(randomExplosive == 1))
                        {
                            tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                    }

                    //폭발성 화약 off
                    else
                    {
                        tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                        Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                        tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        BulletCount--;
                        nowMinRecoil -= recoilAmount;
                        nowMaxRecoil += recoilAmount;

                        if (nowMinRecoil < minRecoil)
                            nowMinRecoil = minRecoil;
                        if (nowMaxRecoil > maxRecoil)
                            nowMaxRecoil = maxRecoil;
                    }

                    if (perks.perk_ThirdArm)
                    {
                        thirdArmPercent = Random.Range(1, 4);
                        if (thirdArmPercent == 1)
                        {
                            tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                            Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                            tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        }
                    }
                source.PlayOneShot(shotSound, 1);
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
                GameObject tmpCart = Instantiate(cart_rifle, cartPoint.position, Quaternion.Euler(0, 0, 0));
                Destroy(tmpCart, 1f);
            }

                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }
            }

            //kar98
            else if (isFiring && BulletCount > 0 && !reloadcheck && WeaponSlot.GetWeaponIdx() == 4)

            {

                if (shotCounter <= 0)
                {
                    if (perks.perk_Asthma == true)
                    {
                        shotCounter = timeBetweenShots - 0.2f;
                    }
                    else
                    {
                        shotCounter = timeBetweenShots;
                    }

                    //폭발성 화약 on
                    if (perks.perk_ExplosiveBullet == true)
                    {
                        randomExplosive = Random.Range(1, 6);
                        if (randomExplosive == 1)
                        {
                            tempMyBullet = Instantiate(bullet_explosive, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                        else if (!(randomExplosive == 1))
                        {
                            tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                    }

                    //폭발성 화약 off
                    else
                    {
                        tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                        Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                        tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        BulletCount--;
                        nowMinRecoil -= recoilAmount;
                        nowMaxRecoil += recoilAmount;

                        if (nowMinRecoil < minRecoil)
                            nowMinRecoil = minRecoil;
                        if (nowMaxRecoil > maxRecoil)
                            nowMaxRecoil = maxRecoil;
                    }

                    if (perks.perk_ThirdArm)
                    {
                        thirdArmPercent = Random.Range(1, 4);
                        if (thirdArmPercent == 1)
                        {
                            tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                            Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                            tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        }
                    }
                source.PlayOneShot(shotSound, 1);
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
                GameObject tmpCart = Instantiate(cart_sniper, cartPoint.position, Quaternion.Euler(0, 0, 0));
                Destroy(tmpCart, 1f);
            }

                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }
            }

            //pump
            else if (isFiring && BulletCount > 0 && WeaponSlot.GetWeaponIdx() == 5)

            {

                if (shotCounter <= 0)
                {
                    shotCounter = timeBetweenShots;

                    int shotgunPellet = 8;

                    //폭발성 화약 on
                    if (perks.perk_ExplosiveBullet == true)
                    {
                        while (shotgunPellet > 0)
                        {
                            randomExplosive = Random.Range(1, 6);
                            Vector3 sp = Camera.main.WorldToScreenPoint(transform.position);
                            Vector3 dir = (Input.mousePosition - sp).normalized;
                            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                            float spread;
                            if (perks.perk_Breacher)
                                spread = Random.Range(-4, 4);
                            else
                                spread = Random.Range(-8, 8);
                            Quaternion bulletRotation = Quaternion.Euler(new Vector3(0, 0, angle + spread));
                            if (randomExplosive == 1)
                            {
                                shotgunBullet = (GameObject)GameObject.Instantiate(bullet_explosive, firePoint.position, bulletRotation);
                            }
                            else if (!(randomExplosive == 1))
                            {
                                shotgunBullet = (GameObject)GameObject.Instantiate(bullet, firePoint.position, bulletRotation);
                            }
                            Vector3 velocity = shotgunBullet.transform.rotation * Vector3.right;
                            shotgunBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            shotgunPellet--;
                        }

                    }

                    //폭발성 화약 off
                    else
                    {
                        // Instantiate the bullet using our new rotation
                        while (shotgunPellet > 0)
                        {
                            //폭발성 화약 off
                            Vector3 sp = Camera.main.WorldToScreenPoint(transform.position);
                            Vector3 dir = (Input.mousePosition - sp).normalized;
                            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                            float spread;
                            if (perks.perk_Breacher)
                                spread = Random.Range(-4, 4);
                            else
                                spread = Random.Range(-8, 8);
                            Quaternion bulletRotation = Quaternion.Euler(new Vector3(0, 0, angle + spread));
                            GameObject shotgunBullet = (GameObject)GameObject.Instantiate(bullet, firePoint.position, bulletRotation);
                            Vector3 velocity = shotgunBullet.transform.rotation * Vector3.right;
                            shotgunBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            shotgunPellet--;
                        }

                    }

                    if (perks.perk_ThirdArm)
                    {
                        thirdArmPercent = Random.Range(1, 4);
                        if (thirdArmPercent == 1)
                        {
                            tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                            Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                            tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        }
                    }

                    BulletCount--;
                    nowMinRecoil -= recoilAmount;
                    nowMaxRecoil += recoilAmount;

                    if (nowMinRecoil < minRecoil)
                        nowMinRecoil = minRecoil;
                    if (nowMaxRecoil > maxRecoil)
                        nowMaxRecoil = maxRecoil;
                source.PlayOneShot(shotSound, 1);
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
                GameObject tmpCart = Instantiate(cart_shotgun, cartPoint.position, Quaternion.Euler(0, 0, 0));
                Destroy(tmpCart, 1f);
            }

                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }
            }

            //de
            else if (isFiring && BulletCount > 0 && !reloadcheck && WeaponSlot.GetWeaponIdx() == 6)

            {

                if (shotCounter <= 0)
                {
                    if (perks.perk_Asthma == true)
                    {
                        shotCounter = timeBetweenShots - 0.02f;
                    }
                    else
                    {
                        shotCounter = timeBetweenShots;
                    }

                    //폭발성 화약 on
                    if (perks.perk_ExplosiveBullet == true)
                    {
                        randomExplosive = Random.Range(1, 6);
                        if (randomExplosive == 1)
                        {
                            tempMyBullet = Instantiate(bullet_explosive, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                        else if (!(randomExplosive == 1))
                        {
                            tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                    }

                    //폭발성 화약 off
                    else
                    {
                        tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                        Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                        tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        BulletCount--;
                        nowMinRecoil -= recoilAmount;
                        nowMaxRecoil += recoilAmount;

                        if (nowMinRecoil < minRecoil)
                            nowMinRecoil = minRecoil;
                        if (nowMaxRecoil > maxRecoil)
                            nowMaxRecoil = maxRecoil;
                    }

                    if (perks.perk_ThirdArm)
                    {
                        thirdArmPercent = Random.Range(1, 4);
                        if (thirdArmPercent == 1)
                        {
                            tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                            Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                            tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        }
                    }
                source.PlayOneShot(shotSound, 1);
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
                GameObject tmpCart = Instantiate(cart_pistol, cartPoint.position, Quaternion.Euler(0, 0, 0));
                Destroy(tmpCart, 1f);
            }

                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }
            }

            //ingram
            else if (isFiring && BulletCount > 0 && !reloadcheck && WeaponSlot.GetWeaponIdx() == 7)

            {

                if (shotCounter <= 0)
                {
                    if (perks.perk_Asthma == true)
                    {
                        shotCounter = timeBetweenShots - 0.02f;
                    }
                    else
                    {
                        shotCounter = timeBetweenShots;
                    }

                    //폭발성 화약 on
                    if (perks.perk_ExplosiveBullet == true)
                    {
                        randomExplosive = Random.Range(1, 6);
                        if (randomExplosive == 1)
                        {
                            tempMyBullet = Instantiate(bullet_explosive, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                        else if (!(randomExplosive == 1))
                        {
                            tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                    }

                    //폭발성 화약 off
                    else
                    {
                        tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                        Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                        tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        BulletCount--;
                        nowMinRecoil -= recoilAmount;
                        nowMaxRecoil += recoilAmount;

                        if (nowMinRecoil < minRecoil)
                            nowMinRecoil = minRecoil;
                        if (nowMaxRecoil > maxRecoil)
                            nowMaxRecoil = maxRecoil;
                    }

                    if (perks.perk_ThirdArm)
                    {
                        thirdArmPercent = Random.Range(1, 4);
                        if (thirdArmPercent == 1)
                        {
                            tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                            Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                            tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        }
                    }
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
                GameObject tmpCart = Instantiate(cart_pistol, cartPoint.position, Quaternion.Euler(0, 0, 0));
                Destroy(tmpCart, 1f);
            }

                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }
            }

            //416
            else if (isFiring && BulletCount > 0 && !reloadcheck && WeaponSlot.GetWeaponIdx() == 8)

            {

                if (shotCounter <= 0)
                {
                    if (perks.perk_Asthma == true)
                    {
                        shotCounter = timeBetweenShots - 0.02f;
                    }
                    else
                    {
                        shotCounter = timeBetweenShots;
                    }

                    //폭발성 화약 on
                    if (perks.perk_ExplosiveBullet == true)
                    {
                        randomExplosive = Random.Range(1, 6);
                        if (randomExplosive == 1)
                        {
                            tempMyBullet = Instantiate(bullet_explosive, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                        else if (!(randomExplosive == 1))
                        {
                            tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                    }

                    //폭발성 화약 off
                    else
                    {
                        tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                        Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                        tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        BulletCount--;
                        nowMinRecoil -= recoilAmount;
                        nowMaxRecoil += recoilAmount;

                        if (nowMinRecoil < minRecoil)
                            nowMinRecoil = minRecoil;
                        if (nowMaxRecoil > maxRecoil)
                            nowMaxRecoil = maxRecoil;
                    }

                    if (perks.perk_ThirdArm)
                    {
                        thirdArmPercent = Random.Range(1, 4);
                        if (thirdArmPercent == 1)
                        {
                            tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                            Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                            tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        }
                    }
                source.PlayOneShot(shotSound, 1);
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
                GameObject tmpCart = Instantiate(cart_rifle, cartPoint.position, Quaternion.Euler(0, 0, 0));
                Destroy(tmpCart, 1f);
            }

                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }
            }

            //mk14
            else if (isFiring && BulletCount > 0 && !reloadcheck && WeaponSlot.GetWeaponIdx() == 9)

            {

                if (shotCounter <= 0)
                {
                    if (perks.perk_Asthma == true)
                    {
                        shotCounter = timeBetweenShots - 0.02f;
                    }
                    else
                    {
                        shotCounter = timeBetweenShots;
                    }

                    //폭발성 화약 on
                    if (perks.perk_ExplosiveBullet == true)
                    {
                        randomExplosive = Random.Range(1, 6);
                        if (randomExplosive == 1)
                        {
                            tempMyBullet = Instantiate(bullet_explosive, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                        else if (!(randomExplosive == 1))
                        {
                            tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                    }

                    //폭발성 화약 off
                    else
                    {
                        tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                        Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                        tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        BulletCount--;
                        nowMinRecoil -= recoilAmount;
                        nowMaxRecoil += recoilAmount;

                        if (nowMinRecoil < minRecoil)
                            nowMinRecoil = minRecoil;
                        if (nowMaxRecoil > maxRecoil)
                            nowMaxRecoil = maxRecoil;
                    }

                    if (perks.perk_ThirdArm)
                    {
                        thirdArmPercent = Random.Range(1, 4);
                        if (thirdArmPercent == 1)
                        {
                            tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                            Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                            tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        }
                    }
                source.PlayOneShot(shotSound, 1);
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
                GameObject tmpCart = Instantiate(cart_sniper, cartPoint.position, Quaternion.Euler(0, 0, 0));
                Destroy(tmpCart, 1f);
            }

                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }
            }

            //m249
            else if (isFiring && BulletCount > 0 && !reloadcheck && WeaponSlot.GetWeaponIdx() == 10)

            {

                if (shotCounter <= 0)
                {
                    if (perks.perk_Asthma == true)
                    {
                        shotCounter = timeBetweenShots - 0.02f;
                    }
                    else
                    {
                        shotCounter = timeBetweenShots;
                    }

                    //폭발성 화약 on
                    if (perks.perk_ExplosiveBullet == true)
                    {
                        randomExplosive = Random.Range(1, 6);
                        if (randomExplosive == 1)
                        {
                            tempMyBullet = Instantiate(bullet_explosive, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                        else if (!(randomExplosive == 1))
                        {
                            tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                    }

                    //폭발성 화약 off
                    else
                    {
                        tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                        Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                        tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        BulletCount--;
                        nowMinRecoil -= recoilAmount;
                        nowMaxRecoil += recoilAmount;

                        if (nowMinRecoil < minRecoil)
                            nowMinRecoil = minRecoil;
                        if (nowMaxRecoil > maxRecoil)
                            nowMaxRecoil = maxRecoil;
                    }

                    if (perks.perk_ThirdArm)
                    {
                        thirdArmPercent = Random.Range(1, 4);
                        if (thirdArmPercent == 1)
                        {
                            tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                            Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                            tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        }
                    }
                //source.PlayOneShot(shotSound, 1);
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
                GameObject tmpCart = Instantiate(cart_rifle, cartPoint.position, Quaternion.Euler(0, 0, 0));
                Destroy(tmpCart, 1f);
            }

                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }
            }

            //pulse
            else if (isFiring && BulletCount > 0 && !reloadcheck && WeaponSlot.GetWeaponIdx() == 11)

            {

                if (shotCounter <= 0)
                {
                    if (perks.perk_Asthma == true)
                    {
                        shotCounter = timeBetweenShots - 0.02f;
                    }
                    else
                    {
                        shotCounter = timeBetweenShots;
                    }

                    //폭발성 화약 on
                    if (perks.perk_ExplosiveBullet == true)
                    {
                        randomExplosive = Random.Range(1, 6);
                        if (randomExplosive == 1)
                        {
                            tempMyBullet = Instantiate(bullet_explosive, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                        else if (!(randomExplosive == 1))
                        {
                            tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                            Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                            tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                            BulletCount--;
                            nowMinRecoil -= recoilAmount;
                            nowMaxRecoil += recoilAmount;

                            if (nowMinRecoil < minRecoil)
                                nowMinRecoil = minRecoil;
                            if (nowMaxRecoil > maxRecoil)
                                nowMaxRecoil = maxRecoil;
                        }
                    }

                    //폭발성 화약 off
                    else
                    {
                        tempMyBullet = Instantiate(bullet, firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + UnityEngine.Random.Range(nowMinRecoil, nowMaxRecoil))) as GameObject;
                        Vector3 velocity = tempMyBullet.transform.rotation * Vector3.right;
                        tempMyBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        BulletCount--;
                        nowMinRecoil -= recoilAmount;
                        nowMaxRecoil += recoilAmount;

                        if (nowMinRecoil < minRecoil)
                            nowMinRecoil = minRecoil;
                        if (nowMaxRecoil > maxRecoil)
                            nowMaxRecoil = maxRecoil;
                    }

                    if (perks.perk_ThirdArm)
                    {
                        thirdArmPercent = Random.Range(1, 4);
                        if (thirdArmPercent == 1)
                        {
                            tempThirdBullet = Instantiate(thirdArmBullet, new Vector3(thirdArmPoint.transform.position.x, thirdArmPoint.transform.position.y, thirdArmPoint.transform.position.z), Quaternion.Euler(0, 0, transform.eulerAngles.z));
                            Vector3 velocity = tempThirdBullet.transform.rotation * Vector3.right;
                            tempThirdBullet.GetComponent<Rigidbody2D>().AddForce(velocity * 10f);
                        }
                    }
                source.PlayOneShot(shotSound, 1);
                GameObject tmpobj = Instantiate(Muzzel[(int)Random.Range(0, Muzzel.Count)], firePoint.position, Quaternion.Euler(0, 0, transform.eulerAngles.z));//as GameObject;
                Destroy(tmpobj, 0.05f);
            }


                if (BulletCount < 0)
                {
                    BulletCount = 0;
                }
            }



        
        nowMinRecoil += restoreRecoilAmount * Time.deltaTime;
		nowMaxRecoil -= restoreRecoilAmount * Time.deltaTime;
		if (nowMinRecoil > 0)
			nowMinRecoil = 0;
		if (nowMaxRecoil < 0)
			nowMaxRecoil = 0;


	}


}