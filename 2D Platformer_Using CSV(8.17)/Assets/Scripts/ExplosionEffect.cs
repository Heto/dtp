﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEffect : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        transform.Rotate(0, 0, Random.Range(0, 360));
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnAnimEnd()
    {
        Destroy(gameObject);
    }

}
