﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleAura : MonoBehaviour {

    public GameObject Tentacle;
    public float speed=0.7f;
    private float step = 0.12f;

    private bool tentacount = true;
    private float delay = 1.0f;
  //  private float afterDelay;
         //private float yVelocity = 0.0F;
    GameObject tenta=null;
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (tentacount)
        {
            if (delay > 0)
            {
                delay -= Time.deltaTime;
            }
            else
            {
               // Debug.Log("gogo");
                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                tenta = Instantiate(Tentacle, transform.position - new Vector3(0,0.2f,0), Quaternion.identity);
              
                tentacount = false;
            }
        }
        else
        {
            if ((transform.position + new Vector3(0, 0.4f, 0)).y <= tenta.transform.position.y)
            {

                Destroy(gameObject, 0.8f);
                Destroy(tenta, 0.8f);
            }
            else
            {
                tenta.transform.position +=new Vector3(0, (speed * Time.deltaTime),0);
                speed += step;
            }
        }

	}
}
