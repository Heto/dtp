﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    
    /***** 성민 : 시작 *****/
    /*
    public enum Kinds
    {
        Claymore,
        AntiTank,
        Spider
    }

    public Kinds kind;

    void Start()
    {
    }
    */
    /***** 성민 : 종료 *****/
  
    public int minetype;  // 1=normal , 2=heavy
	public BoomTrigger boomTrigger;

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Enemy")
		{
			if (minetype == 1)
			{
				Instantiate(boomTrigger, new Vector2(transform.position.x, transform.position.y + 0.61f), transform.rotation);
				Destroy(gameObject);
			}
			else if (minetype == 2)
			{
				if (col.gameObject.GetComponent<EnemyController>().EnemyType == 4)
				{
					Instantiate(boomTrigger, new Vector2(transform.position.x, transform.position.y + 0.61f), transform.rotation);
					Destroy(gameObject);
				}
			}
		}
	}

}
