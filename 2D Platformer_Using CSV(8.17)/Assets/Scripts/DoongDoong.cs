﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoongDoong : MonoBehaviour {

    public Vector3 firstPos;
    public float angle;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        DoongDoongEffect();
    }

    void DoongDoongEffect()
    {
        Vector3 pos = transform.position;
        pos.y += 0.003f * Mathf.Sin(angle);
        angle += 2.5f * Time.deltaTime;
        if (angle > 180.0f)
        {
            angle = 0.0f;
        }

        transform.position = pos;
    }
}
