﻿/*
 * using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderMineController : MonoBehaviour {

	public bool isLeft;
	public EnemyMove Move;
	private Vector2 target1; // left
	private Vector2 target2; // right
	private Vector2 Maintarget;

	public float condition;
	//private Transform closetEnemytrans = null;
	private float distance = 100f;

	void Awake()
	{
		isLeft = true;
		target1 = new Vector2(-12, transform.position.y);
		target2 = new Vector2(13, transform.position.y);
		Move = this.gameObject.GetComponent<EnemyMove>();
		if (transform.position.x > 0)
			Maintarget = target2;
		else
			Maintarget = target1;
	}
	void Update()
	{
		
		Move.GetTarget(Maintarget);
		isLeft = Move.MoveToTarget();
		Move.flipNormal(isLeft);
	}
	void ChangeTarget()
	{
		if (Maintarget == target1)
		{
			Maintarget = target2;
		}
		else
		{
			Maintarget = target1;
		}
	}
	void tracing()
	{
		GameObject[] targetenemys = GameObject.FindGameObjectsWithTag("enemy");
		//Transform closetEnemy = null;
		foreach (GameObject targetenemy in targetenemys)
		{
			distance=Vector2.Distance(targetenemy.transform.position, transform.position);
			if (distance < condition)
			{
				//실행
			}
		}

	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "barricade")
		{
			ChangeTarget();
		}
	}
}
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderMineController : MonoBehaviour
{

    public bool isLeft;
    public EnemyMove Move;
    private Vector2 target1; // left
    private Vector2 target2; // right
    private Vector2 mainTarget; // selected direction

    //not inspector
    private int myPhase;
    private float myEnergy;
    private float stunAnimTime; // stun animation time 고정값 : 2f
    private float stunAnimDelayTime; // 스턴 후딜레이
    private float stunCoolTime; // 스턴 쿨타임
    private float moveEnergy;


    //inspector
    public GameObject stunAttack;
    public float condition; // 공격 가능 거리 조건



    private float distance = 100f;

    void Awake()
    {
        myPhase = 1;
        myEnergy = 100;
        stunAnimDelayTime = 1f;
        stunCoolTime = 0;
       // condition = 3.3f;
        stunAnimTime = 2f;
        moveEnergy = 100f;
        isLeft = true;
        target1 = new Vector2(-12, transform.position.y);
        target2 = new Vector2(13, transform.position.y);
        Move = this.gameObject.GetComponent<EnemyMove>();
        if (transform.position.x > 0)
            mainTarget = target2;
        else
            mainTarget = target1;
    }
    void Update()
    {
        PhaseManager();
    }
    void PhaseManager()
    {
        if (myPhase == 1) // 이동단계
        {
            if (moveEnergy > 0)
            {
                Move.GetTarget(mainTarget); // 좌우 설정
                isLeft = Move.MoveToTarget(); // 이동
                Move.flipNormal(isLeft); //flip 설정
            }
            else
            {
                RandomChangeTarget();
                moveEnergy = Random.Range(60f, 120f);
            }

            if (stunCoolTime > 0)
            {
                stunCoolTime -= Time.deltaTime;
            }
            else
            {
                Tracing(); // 주변 enemy 탐지     foreach 사용 , box도 가능할듯
            }
        }
        else if (myPhase == 2) //공격 animation 및 공격
        {
            StunShot();
        }

        else if (myPhase == 3)
        {
            if (stunAnimDelayTime > 0)
                stunAnimDelayTime -= Time.deltaTime;
            else
            {
                stunAnimDelayTime = 1f;
                myPhase = 1;
            }
            //stop
        }
        else if (myPhase == 4) // energy charge
        {

            Charge();
        }
    }
    void ChangeTarget()
    {
        if (mainTarget == target1)
        {
            mainTarget = target2;
        }
        else
        {
            mainTarget = target1;
        }
    }



    void RandomChangeTarget()
    {
        if (Random.Range(0.0f, 1.0f) < 0.2f)
        {
            ChangeTarget();
        }

    }

    void Tracing()
    {
        GameObject[] targetenemys = GameObject.FindGameObjectsWithTag("Enemy");
        //Transform closetEnemy = null;
        foreach (GameObject targetenemy in targetenemys)
        {
            distance = Vector2.Distance(targetenemy.transform.position, transform.position);
            if (distance < condition)
            {
                myPhase = 2;
            }
        }

    }


    void StunShot()
    {
        if (stunAnimTime > 0)
        {
            stunAnimTime -= Time.deltaTime;
        }
        else
        {
            GameObject stunArea = Instantiate(stunAttack, transform.position, Quaternion.identity);
            Destroy(stunArea, 1.0f);
            stunCoolTime = 2f;
            myEnergy -= 20;
            myPhase = 3;
            stunAnimTime = 2f;
        }
    }

    void Charge()
    {
        if (myEnergy < 99)
        {
            myEnergy += Time.deltaTime;
        }
        else
        {
            myPhase = 1;
        }

    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "barricade")
        {
            ChangeTarget();
        }
    }
}
