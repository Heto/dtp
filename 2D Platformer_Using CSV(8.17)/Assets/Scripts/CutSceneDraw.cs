﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneDraw : MonoBehaviour {

    public int currentScene = 0;
    public GameObject scene1;
    public GameObject scene2;
    public GameObject scene3;
    public GameObject scene4;
    public GameObject scene5;
    public GameObject scene6;
    public GameObject scene7;
    public GameObject scene8;
    public GameObject scene9;
    public GameObject scene10;
    public GameObject scene11;
    public GameObject scene12;
    public GameObject scene13;
    public GameObject scene14;
    public GameObject scene15;
    public GameObject scene16;
    public GameObject scene17;
    public LevelChanger lech;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
            OnScreenClicked();
	}

    public void OnScreenClicked()
    {
        if (currentScene == 0)
            scene1.SetActive(true);
        else if (currentScene == 1)
            scene2.SetActive(true);
        else if (currentScene == 2)
            scene3.SetActive(true);
        else if (currentScene == 3)
            scene4.SetActive(true);
        else if (currentScene == 4)
            scene5.SetActive(true);
        else if (currentScene == 5)
            scene6.SetActive(true);
        else if (currentScene == 6)
            scene7.SetActive(true);
        else if (currentScene == 7)
            scene8.SetActive(true);
        else if (currentScene == 8)
            scene9.SetActive(true);
        else if (currentScene == 9)
            scene10.SetActive(true);
        else if (currentScene == 10)
            scene11.SetActive(true);
        else if (currentScene == 11)
            scene12.SetActive(true);
        else if (currentScene == 12)
            scene13.SetActive(true);
        else if (currentScene == 13)
            scene14.SetActive(true);
        else if (currentScene == 14)
            scene15.SetActive(true);
        else if (currentScene == 15)
            scene16.SetActive(true);
        else if (currentScene == 16)
            scene17.SetActive(true);
        else if (currentScene == 17)
        {
            lech.FadeToLevel(2);
        }
        currentScene++;
    }
}
