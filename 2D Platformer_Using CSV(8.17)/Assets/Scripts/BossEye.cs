﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEye : MonoBehaviour
{

    public float Hp = 200f;

    public GameObject BossGuard;

    public Transform connectedAnchorPos;
  
    public Rigidbody2D rb2d;
    public float leftPushRange;
    public float rightPushRange;
    public float velocityThreshold;


    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.angularVelocity = velocityThreshold;
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.GetComponent<HingeJoint2D>().connectedAnchor = new Vector2(connectedAnchorPos.transform.position.x, connectedAnchorPos.transform.position.y);

        if (Hp < 0)
        {
            Destroy(BossGuard);
            Destroy(this.gameObject);

        }
        Push();
    }
    void Push()
    {
        if (transform.rotation.z > 0 && transform.rotation.z < rightPushRange &&
            (rb2d.angularVelocity > 0) && rb2d.angularVelocity < velocityThreshold)
        {
            rb2d.angularVelocity = velocityThreshold*1;
        }
        else if (transform.rotation.z < 0 && transform.rotation.z > leftPushRange &&
            (rb2d.angularVelocity < 0) && rb2d.angularVelocity > velocityThreshold * -1)
        {
            rb2d.angularVelocity = velocityThreshold * -1;
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bullet")
        {
            Destroy(collision.gameObject);
            Hp -= 10f;
        }
    }

}
