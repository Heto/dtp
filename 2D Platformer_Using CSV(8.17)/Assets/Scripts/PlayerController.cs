﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{

	public float moveSpeed;
	private float activeMoveSpeed;
    public PerksManager perks;

    //체력
    public float hp;
    public float staticHp;
    public float hpDiscount;
    public Image hpGage;

	//산소
	public float oxygenpoint;
	public float oxygenpointDiscount;
	public float perk_Rashness_oxygenpointDiscount;
	public Image oxygenGage;

	//스테미너
	public float stamina;
	public Image staminaGage;
	public float refillStaminatime;
	private float startrefillStamina;
	private bool refillstaminaCheck;

	//몬스터 처치시 에너지 획득
	public int energy;
	//몬스터 처치시 누적(광전사)
	public int accumulate_num;

	public bool canMove;
    public bool moveCheck;

    public float hatchOpenSpeed;
    public float barricadeFixSpeed = 2f;

    public Rigidbody2D myRigidbody;

	public float jumpSpeed;

	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;

	public bool isGrounded;

	//private Animator myAnim;

	public Vector3 respawnPosition;
    
	public float knockbackForce;
	public float knockbackLength;
	private float knockbackCounter;

	public float invincibilityLength;
	private float invincibilityCounter;
    
	private bool onPlatform;
	public float onPlatformSpeedModifier;

	public GameObject pistol;
	public GameObject mp5;
	public GameObject shotgun;
	public GameObject ak;
	public GameObject kar98;
	public GameObject pump;
    public GameObject de;
    public GameObject ingram;
    public GameObject hk416;
    public GameObject mk14;
    public GameObject m249;
    public GameObject pulse;
    public GameObject grenade;
    public GameObject caltrop;
    public GameObject caltrop2;
	public int whatItem;
   
    public bool takeUpstair;
	public bool takeDownstair;

    public int nowHeight;

    public bool adrenalineOn = false;
    public int braveCount = 0;
    public Transform caltropPosition;
    public bool hatchOpened = false;

	Transform playerGraphics;

    public GameObject gameOverUI;
    public GameObject pauseUI;
    public bool isGameOvered = false;

    //////////////////////// < D A S H >
    private int walkDirection;

    public float rollSpeed;
    public bool rolling;
    public GameObject playerGraphic;
    public bool rollstart;
    public Anim anim;
    public bool agilityUsed = false;
    public AudioClip dashSound;
    public AudioSource source;
    ///////////////////////// </ D A S H >

    // 변신데미지
    private float _transfomDamage = 1.0f;
    public float transformDamage
    {
        get { return _transfomDamage; }
        set
        {
            if (value >= 0)
                _transfomDamage = value;
        }
    }

    // 에너지 획득량
    public int amountObtainEnergy = 0;

    // 아이템
    public GameObject item = null;

    void UseItem()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (item != null)
            {
                item.GetComponent<Item>().Use();
            }
        }
    }

    void Awake()
	{
        if (dashSound != null)
        {
            source = GetComponent<AudioSource>();
        }
        playerGraphics = transform.Find("Graphics");
		if (playerGraphics == null)
		{
			Debug.LogError("There is no 'Graphics' object as a child of the player");
		}
	}

	// Use this for initialization
	void Start()
	{
        //UI에 띄우기 위한 초기 hp값 대입
        staticHp = hp;
        hpDiscount = 0;
        nowHeight = 1;

        myRigidbody = GetComponent<Rigidbody2D>();
		//myAnim = GetComponent<Animator>();

		respawnPosition = transform.position;

		
		activeMoveSpeed = moveSpeed;

		canMove = true;

		takeUpstair = false;
		takeDownstair = false;

		refillstaminaCheck = false;
		perks = GameObject.Find("Player").GetComponent<PerksManager>();

        rolling = false;
        rollstart = false;

    }

	// Update is called once per frame
	void Update()
	{

        if (hp > staticHp)
            hp = staticHp;
		
		//몬스터 처치시 누적(광전사)
		if (accumulate_num > 30)
		{
			accumulate_num = 30;
		}               

		isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        if (knockbackCounter <= 0 && canMove && isGameOvered == false)
        {

            if (onPlatform)
            {
                activeMoveSpeed = moveSpeed * onPlatformSpeedModifier;
            }
            else
            {
                activeMoveSpeed = moveSpeed;
            }

            if (Input.GetAxisRaw("Horizontal") > 0f)
            {
                moveCheck = true;
                myRigidbody.velocity = new Vector3(activeMoveSpeed, myRigidbody.velocity.y, 0f);
                walkDirection = 2;
            }

            else if (Input.GetAxisRaw("Horizontal") < 0f)
            {
                moveCheck = true;
                myRigidbody.velocity = new Vector3(-activeMoveSpeed, myRigidbody.velocity.y, 0f);
                walkDirection = 1;
            }
            else
            {
                myRigidbody.velocity = new Vector3(0f, myRigidbody.velocity.y, 0f);
                moveCheck = false;
            }
        }
        else if (isGameOvered)
        {
            myRigidbody.velocity = new Vector3(0f, myRigidbody.velocity.y, 0f);
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            hp -= 50;
            if (hp <= 0)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().followTarget = true;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().bossroomFollowTarget = false;
                gameOverUI.SetActive(true);
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (pauseUI.activeSelf)
            {
                pauseUI.SetActive(false);
                Time.timeScale = 1f;
            }
            else
            {
                pauseUI.SetActive(true);
                Time.timeScale = 0f;
            }
        }
        
        if (whatItem == 1)
		{
			grenade.SetActive(true);
		}


		//산소 게이지
		oxygenGage.fillAmount = oxygenpoint / 100;
		oxygenpointDiscount = Time.deltaTime * 1f;
		perk_Rashness_oxygenpointDiscount += oxygenpointDiscount;
		oxygenpoint -= oxygenpointDiscount;

		hpGage.fillAmount = hp / staticHp;


        if (oxygenpoint < 0)
        {
            oxygenpoint = 0;
        }

        if (oxygenpoint == 0)
        {
            //discountHP -= Time.deltaTime * 2f;
            //hp = (int)discountHP;
            hpDiscount += Time.deltaTime * 2f;

            if (hpDiscount >= 2f)
            {
                hp -= 1;
                hpDiscount = 0;
            }

            if (hp < 0)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().followTarget = true;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().bossroomFollowTarget = false;

                hp = 0;
            }
        }

        if (takeUpstair == true)
		{
            transform.position = new Vector3(transform.position.x - 8f,transform.position.y + 1.35f, 0);
			takeUpstair = false;
		}

		if (takeDownstair == true)
		{
			transform.position = new Vector3(transform.position.x + 8f,transform.position.y - 1.35f, 0);
			takeDownstair = false;
		}

		if (knockbackCounter > 0)
		{
			knockbackCounter -= Time.deltaTime;

			if (transform.localScale.x > 0)
			{
				myRigidbody.velocity = new Vector3(-knockbackForce, knockbackForce, 0f);
			}
			else
			{
				myRigidbody.velocity = new Vector3(knockbackForce, knockbackForce, 0f);
			}
		}

		if (invincibilityCounter > 0)
		{
			invincibilityCounter -= Time.deltaTime;
		}
        				

		//구르기
		//////// D A S H /////////////////////////////////
		staminaGage.fillAmount = stamina / 3;
		
        //if (stamina > 0 && Input.GetMouseButtonDown(1) && rolling == false)
        //{
        //    canMove = false;
        //    rollSpeed = 8f;
        //    rolling = true;
        //    stamina--;
        //}

        if (rolling)
        {

               
            if (walkDirection == 1)
            {
                transform.position = new Vector2(transform.position.x - rollSpeed * Time.deltaTime, transform.position.y);
                playerGraphic.transform.localScale = new Vector3(-1f, 1f, 1f);
            }                
            else
            {
                transform.position = new Vector2(transform.position.x + rollSpeed * Time.deltaTime, transform.position.y);
                playerGraphic.transform.localScale = new Vector3(1f, 1f, 1f);
            }

            Physics2D.IgnoreLayerCollision(9, 19, true);
            rollSpeed -= rollSpeed * 8f * Time.deltaTime;
            
            if (rollSpeed < 0.5f)
            {
                rolling = false;
                canMove = true;
                anim.canRoll = true;
                Physics2D.IgnoreLayerCollision(9, 19, false);
            }
        }

        if (stamina < 3)
		{
			refillstaminaCheck = true;
            if (agilityUsed)
                startrefillStamina += Time.deltaTime * 5f;
            else
                startrefillStamina += Time.deltaTime;

            if (refillStaminatime <= startrefillStamina && refillstaminaCheck)
			{
				refillstaminaCheck = false;
				startrefillStamina = 0;
				stamina++;
			}
		}
        ////////////////////D A S H/ /////////////////////////
        
        UseItem();
    }

    public void Knockback()
	{
		knockbackCounter = knockbackLength;
		invincibilityCounter = invincibilityLength;
		
	}


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "KillPlane")
		{
			//gameObject.SetActive(false);

			//transform.position = respawnPosition;

			
		}

		if (other.tag == "Checkpoint")
		{
			respawnPosition = other.transform.position;
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "MovingPlatform")
		{
			transform.parent = other.transform;
			onPlatform = true;
		}
	}

	void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.tag == "MovingPlatform")
		{
			transform.parent = null;
			onPlatform = false;
		}
	}

    void LateUpdate()
    {
        if (stamina > 0 && Input.GetMouseButtonDown(1) && rolling == false && isGameOvered == false)
        {
            if (perks.perk_Ninja)
            {
                int ranCaltrop = Random.Range(1, 3);

                if(ranCaltrop == 1)
                    Instantiate(caltrop, caltropPosition.transform.position, transform.rotation);
                else
                    Instantiate(caltrop2, caltropPosition.transform.position, transform.rotation);
            }
            canMove = false;
            rollSpeed = 8f;
            rolling = true;
            source.PlayOneShot(dashSound, 1.0f);
            stamina--;
        }
    }

    public void GetDamage(float dmg)
    {
        if (adrenalineOn)
        {
            hp -= dmg / 2;
            adrenalineOn = false;
        }
        else if (braveCount > 0)
        {
            braveCount--;
        }
        else
            hp -= dmg;

        if (perks.perk_Berserker)
        {
            accumulate_num = 0;
        }

        if (hp <= 0)
        {
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().followTarget = true;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().bossroomFollowTarget = false;

            gameOverUI.SetActive(true);
        }
    }


}