﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetPerkText : MonoBehaviour {

    public TextMesh text;
    public Animator anim;
    public Vector3 newPos;
    public PerkBox perkbox;
    private float fadeStartTime = 0;

	// Use this for initialization
	void Start () {
        text = GetComponent<TextMesh>();
        //anim = GetComponent<Animator>();
        perkbox = FindClosestEnemy().GetComponent<PerkBox>();
        Destroy(gameObject, 3.5f);
        GetPerkNum();
    }

    public GameObject FindClosestEnemy()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("PerkBox");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }

    // Update is called once per frame
    void Update () {
        transform.Translate(Vector2.up * 0.02f * Time.deltaTime);
        fadeStartTime++;
        if (fadeStartTime >= 80f)
            text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - (Time.deltaTime / 2f));
	}

    private void GetPerkNum()
    {
        switch(perkbox.randomPerk)
        {
            case 1: text.text = "근육질 : 문을 열기 더 수월해진 것 같다"; break;
            case 2: text.text = "공돌이 : 수리는 내 전문"; break;
            case 3: text.text = "식인 : 신선한... 고기!"; break;
            case 4: text.text = "천식 : 콜록... 숨쉬기가 힘들어졌다"; break;
            case 5: text.text = "세번째 팔 : 등에 뭔가 솟아났다..."; break;
            case 6: text.text = "터렛 장인 : 너한테 거는 기대가 크구나!"; break;
            case 7: text.text = "유틸리티 벨트 : 주머니가 넉넉해졌다."; break;
            case 8: text.text = "초크 : 샷건의 사거리가 늘어났다."; break;
            case 9: text.text = "히트맨 : 권총 전문가"; break;
            case 10: text.text = "포인트맨 : 기관단총을 다루기 쉬워졌다"; break;
            case 11: text.text = "라이플맨 : 소총에 더 큰 탄창을 달았다"; break;
            case 12: text.text = "인내심 : 더 많은 고통을 참을 수 있다"; break;
            case 13: text.text = "흥정 : 좀 깎아주세요~"; break;
            case 14: text.text = "보정기 : 빗맞아도 걱정 없어!"; break;
            case 15: text.text = "폭발성 화약 : 펑 펑 터져라!"; break;
            case 16: text.text = "트리플베이스 화약 : 더 빨리 날아가는 총알"; break;
            case 17: text.text = "아드레날린 : 계속 죽여서 버텨!"; break;
            case 18: text.text = "침착함 : 멈춰 있을 때 가장 조준하기 쉽지"; break;
            case 19: text.text = "광전사 : 죽일수록 강해진다"; break;
            case 20: text.text = "희망 : 문을 열 수록 피어나는 희망"; break;
            case 21: text.text = "닌자 : 밟으면 아플걸?"; break;
            case 22: text.text = "절약정신 : 박힌 총알도 다시 보자"; break;
            case 23: text.text = "고함 : 푸스 로 다!"; break;
            case 24: text.text = "무모함 : 위험을 즐김"; break;
            case 25: text.text = "학살자 : 끊임없이 죽여!"; break;
            case 26: text.text = "확장 탄창 : 탄창이 길어진 것 같다"; break;
            case 27: text.text = "철갑탄 : 갑옷도 뚫는 관통력"; break;
            case 28: text.text = "스나이퍼 : 저격총을 더 효율적으로 다루게 되었다"; break;
        }
        
    }
    
    void OnAnimEnd()
    {
        //Destroy(gameObject);
    }
}
