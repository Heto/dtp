﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour {

    private Rigidbody2D rb;
    private Vector3 aimAngle;
    public BoomTrigger boomTrigger;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        Throwing();
	}
	
	// Update is called once per frame
	void Update () {           

    }

    void Throwing()
    {
        Vector3 sp = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 dir = (Input.mousePosition - sp).normalized;
        rb.AddForce(dir * 400f);
        //aimAngle = (Mathf.Atan2(m_pos.y, m_pos.x) * Mathf.Rad2Deg);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Ground")
        {
            Instantiate(boomTrigger, new Vector2(transform.position.x, transform.position.y + 0.7f), transform.rotation);
            Destroy(gameObject);
        }
    }
}
