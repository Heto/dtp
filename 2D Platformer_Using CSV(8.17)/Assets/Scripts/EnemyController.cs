﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public int EnemyType; // 1 normal , 2 range , 3 fast , 4 boom
    private int attackOrder;
    public float hp;
    public PlayerController player;
    public GameObject playerObject;
    public Weapon weapon;
    public EnemyMove Move;
    public EnemyMeleeAttack NormalAttack;
   // public EnemyRangeAttack RangeAttack;
    public EnemyDie Die;

    public GameObject bulletEffect;

    protected Animator myAnim;

    private bool IsLeft;
    private bool IsMove;
    private float distance; // player between enemy distance
    private int playerIsUpperThanEnemy;
    public PerksManager perks;



    private BoxCollider2D[] myBox;
    public Rigidbody2D rgd;

    // Use this for initialization
    void Start()
    {
        myBox = GetComponentsInChildren<BoxCollider2D>();
        IsLeft = true;
        IsMove = true;
        Move = this.gameObject.GetComponent<EnemyMove>();
        NormalAttack = this.gameObject.GetComponent<EnemyMeleeAttack>();
        /*
        if (EnemyType == 2)
            RangeAttack = this.gameObject.GetComponent<EnemyRangeAttack>();
        else
            RangeAttack = null;
            */
        Die = this.gameObject.GetComponent<EnemyDie>();
        Die.EnemyType = EnemyType;
        myAnim = GetComponent<Animator>();
        player = FindObjectOfType<PlayerController>();
        perks = GameObject.Find("Player").GetComponent<PerksManager>();
        attackOrder = 1;
        rgd = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {

       // Debug.Log(NormalAttack.dashOn);
        Die.DieCheck(hp, myAnim,myBox);
        if (StateCheck())
        {
            MoveAction();
            AttackAction();
        }
        else
        {
            Move.CircularTimer();
        }

        ////////////////////////////////<<<<<<<<<<<<<<<<<<<<<<<<<< 진영 수정했음 
        if (player.isGameOvered)
        {
            myAnim.enabled = false;
        }
        ////////////////////////////////<<<<<<<<<<<<<<<<<<<<<<<<<< 여기까지

        /*
		if (StateCheck())
		{
			MoveAction();
			AttackAction();
			Die.DieCheck(hp, myAnim);
			//Debug.Log(distance);
		}
		else
		{
			Move.CircularTimer();
		}

		MoveAction();
		AttackAction();
		Die.DieCheck(hp, myAnim);
        */

        //Debug.Log(distance);
    }

    void LateUpdate()
    {
        weapon = WeaponSlot.GetWeapon();
    }
    public void AnimDieEnd()
    {
        Destroy(gameObject);
    }
    bool StateCheck()
    {
        if (Move.isSturn)
        {
            NormalAttack.attackTrigger.enabled = false;
            return false;
        }
        else if (Die.isDying)
        {
            NormalAttack.attackTrigger.enabled = false;
            return false;
        }
        return true;
    }

    public void MoveAction()
    {
        if (IsMove && player.isGameOvered == false) // 이동가능 <<<<<<<<<<<<<<<<<<<<<<<<<<<< 진영 수정했음 (player.isGameOvered 추가) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        {
            playerIsUpperThanEnemy = Move.FindPlayer(player); // player 위치 찾기
            distance = Move.SetToTarget(playerIsUpperThanEnemy, player);//player 와의 거리 계산후 이동할 타겟선택
         
                IsLeft = Move.MoveToTarget();// player에게 이동 및 flip을 위한 bool 변수 받아옴
      
            Move.flip(IsLeft); // flip
        }
    }

    public void EndAttack()
    {
        distance = 30f;
        //Debug.Log("212121");
        NormalAttack.DeleteAttackAnim(myAnim);
    }
    public void EndDash()
    {
        distance = 30f;
        NormalAttack.DeleteDashAttackAnim(myAnim);
    }
    public void DashOn()
    {
        NormalAttack.DashStartAnim(IsLeft);
    }
    public void TestMove()
    {
        NormalAttack.TestDash(IsLeft);
    }

    public void ShootEndInit()
    {
        NormalAttack.ShootAnimEnd(myAnim);
    }
    public void ShootBullet()
    {
       NormalAttack.ShootingBullet(IsLeft);
    }
    public void BoomerAttack()
    {
        Die.BoomerAttackStart(hp, myBox);
    }

    public void AttackAction()
    {
        if (attackOrder == 1)
        {

            NormalAttack.CheckingAttack(distance);
            IsMove = !NormalAttack.AttackStart;
            if (NormalAttack.AttackStart)
            {
               // if(!myAnim.GetBool("attack"))
                NormalAttack.MeleeAttackAnim(myAnim);
            }
  
        }
        else if (attackOrder == 2)
        {
            NormalAttack.CheckingAttack(distance);
            IsMove = !NormalAttack.AttackStart;
            if (NormalAttack.AttackStart)
            {
                //RangeAttack.ShootBullet(IsLeft);
                // NormalAttack.MeleeAttackAnim(myAnim);
                NormalAttack.ShootAnimStart(myAnim);
            }
        }
        else if (attackOrder == 3)
        {
            NormalAttack.CheckingAttack(distance);
            IsMove = !NormalAttack.AttackStart;
            if (NormalAttack.AttackStart)
            {
                //if (!myAnim.GetBool("attackDash"))
                    NormalAttack.MeleeDashAttackAnim(myAnim);
               // NormalAttack.MeleeAttackAnim(myAnim);
            }
        }
    }


    public void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Upstair")
        {
            if (playerIsUpperThanEnemy == 2)
            {
                //x - 11.24
                // y + 4.04
                //transform.position = new Vector3(transform.position.x - 11.24f, transform.position.y + 4.04f, 0f);
                transform.position = new Vector3(transform.position.x - 8f, transform.position.y + 1.35f, 0);
            }
        }

        if (col.gameObject.tag == "downstair")
        {
            if (playerIsUpperThanEnemy == 0)
            {
                //transform.position = new Vector3(transform.position.x + 11.24f, transform.position.y - 4.02f, 0f);
                transform.position = new Vector3(transform.position.x + 8f, transform.position.y - 1.35f, 0);
            }
        }
        if (col.gameObject.tag == "barricade")
        {
            if (col.gameObject.GetComponent<Barricade>().brokenNow())
            {
                //IsMove = true;
                if (myAnim.GetBool("move"))
                {
                    IsMove = true;
                    NormalAttack.AttackStart = false;
                }

                if (EnemyType == 2)
                {
                    //RangeAttack.condition = 2f;
                    NormalAttack.condition = 4f;
                    distance = 10f;
                    attackOrder = 2;
                }
                else if (EnemyType == 3)
                {
                    distance = 10f;
                    attackOrder = 3;
                }
                else
                {
                    //NormalAttack = 0.1f;
                    distance = 10;
                    attackOrder = 1;
                }
            }
            else
            {

                IsMove = false;
                distance = 0;
                attackOrder = 1;
                NormalAttack.AttackStart = true;
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            }

        }

    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            if (WeaponSlot.GetWeaponIdx() == 0 && perks.perk_Hitman == true)
            {
                if (weapon.perk_Slayer_Check)
                    hp -= 2.0f * (weapon.damage + 10.0f);
                else
                    hp -= (weapon.damage + 20.0f);

                weapon.perk_Slayer_Check = false;
            }
            else if (WeaponSlot.GetWeaponIdx() == 6 && perks.perk_Hitman == true)
            {
                if (weapon.perk_Slayer_Check)
                    hp -= 2.0f * (weapon.damage + 10.0f);
                else
                    hp -= (weapon.damage + 50.0f);

                weapon.perk_Slayer_Check = false;
            }

            else
            {
                if (weapon.perk_Slayer_Check)
                    hp -= 2.0f * weapon.damage;

                else
                    hp -= weapon.damage;

                weapon.perk_Slayer_Check = false;
            }


            Instantiate(bulletEffect, col.transform.position, col.transform.rotation);
            StartCoroutine(HitEffect());
            //Destroy(col.gameObject);
        }

        if (col.gameObject.tag == "ThirdArmBullet")
        {
            hp -= 20;
            Instantiate(bulletEffect, col.transform.position, col.transform.rotation);
            StartCoroutine(HitEffect());
            Destroy(col.gameObject);
        }

        if (col.gameObject.tag == "barricade1")
        {
            IsMove = false;
        }

        if (col.gameObject.tag == "ExplosionGrenade")
        {
            hp -= 100;

        }

        if (col.gameObject.tag == "ExplosionBullet")
        {
            hp -= 30;

        }
        if (col.gameObject.tag == "Caltrop")
        {
            hp -= 50;
            Destroy(col.gameObject);
        }

        if (col.gameObject.tag == "Boom")
        {
            hp -= 400;

        }
        if (col.gameObject.tag == "Stun")
        {
            hp -= 20;
            Move.GetStun();

        }
        if (col.gameObject.tag == "Shout")
        { 
            if (col.gameObject.transform.position.x > transform.position.x)
                rgd.velocity = new Vector2(-5f, 0);
            else
                rgd.velocity = new Vector2(5f, 0);

        }


    }
    IEnumerator HitEffect()
    {
        Renderer rend = GetComponent<Renderer>();

        rend.material.color = Color.red;
        yield return new WaitForSeconds(0.04f);
        rend.material.color = Color.white;
        yield return new WaitForSeconds(0.04f);

    }
}
