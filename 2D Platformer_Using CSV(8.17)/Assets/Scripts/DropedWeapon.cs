﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropedWeapon : MonoBehaviour
{

    public PlayerController player;   
    public int weaponNum;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                if (weaponNum == 1)
                {
                    WeaponSlot.AcquireWeapon(WeaponSlot.Kinds.mp5, true);
                    Destroy(gameObject);
                }
                if (weaponNum == 2)
                {
                    WeaponSlot.AcquireWeapon(WeaponSlot.Kinds.doubleBarrel, true);
                    Destroy(gameObject);
                }
				if (weaponNum == 3)
				{
                    WeaponSlot.AcquireWeapon(WeaponSlot.Kinds.ak, true);
					Destroy(gameObject);
				}
				if (weaponNum == 4)
				{
                    WeaponSlot.AcquireWeapon(WeaponSlot.Kinds.kar98, true);
					Destroy(gameObject);
				}
				if (weaponNum == 5)
				{
                    WeaponSlot.AcquireWeapon(WeaponSlot.Kinds.pump, true);
                    Destroy(gameObject);
				}
                if (weaponNum == 6)
                {
                    WeaponSlot.AcquireWeapon(WeaponSlot.Kinds.DE, true);
                    Destroy(gameObject);
                }
                if (weaponNum == 7)
                {
                    WeaponSlot.AcquireWeapon(WeaponSlot.Kinds.ingram, true);
                    Destroy(gameObject);
                }
                if (weaponNum == 8)
                {
                    WeaponSlot.AcquireWeapon(WeaponSlot.Kinds.hk416, true);
                    Destroy(gameObject);
                }
                if (weaponNum == 9)
                {
                    WeaponSlot.AcquireWeapon(WeaponSlot.Kinds.mk14, true);
                    Destroy(gameObject);
                }
                if (weaponNum == 10)
                {
                    WeaponSlot.AcquireWeapon(WeaponSlot.Kinds.m249, true);
                    Destroy(gameObject);
                }
                if (weaponNum == 11)
                {
                    WeaponSlot.AcquireWeapon(WeaponSlot.Kinds.pulsegun, true);
                    Destroy(gameObject);
                }

            }

        }

    }
        

}
