﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSlot : MonoBehaviour {
    public enum Kinds
    {
        pistol = 0,
        mp5 = 1<<0,
        doubleBarrel = 1<<1,
        ak = 1<<2,
        kar98 = 1<<3,
        pump = 1<<4,
        DE = 1<<5,
        ingram = 1<<6,
        hk416 = 1<<7,
        mk14 = 1<<8,
        m249 = 1<<9,
        pulsegun = 1<<10,
    }
    public GameObject pistol;

    private static PlayerController player;
    public static List<GameObject> slotList;
    public static int slotListIdx;

    public static GameObject slotUI;
    public static GameObject InstSlotUI;

    // Use this for initialization
    void Awake () {
        slotList = new List<GameObject>() { pistol };
        slotUI = GameObject.Find("WeaponSlot");
        ChangeSlot(true);
    }
	
	// Update is called once per frame
	void Update () {
        ChangeWeapon();
    }

    // DropedWeapon.cs에서 무기 획득시 호출
    public static void AcquireWeapon(Kinds weapon, bool changeSlotFalse)
    {
        if (player == null)
            player = GameObject.FindObjectOfType<PlayerController>();

        if (changeSlotFalse)
            ChangeSlot(false);

        switch (weapon)
        {
            case Kinds.mp5:
                {
                    _AcquireWeapon(player.mp5);
                    break;
                }
            case Kinds.doubleBarrel:
                {
                    _AcquireWeapon(player.shotgun);
                    break;
                }
            case Kinds.ak:
                {
                    _AcquireWeapon(player.ak);
                    break;
                }
            case Kinds.kar98:
                {
                    _AcquireWeapon(player.kar98);
                    break;
                }
            case Kinds.pump:
                {
                    _AcquireWeapon(player.pump);
                    break;
                }
            case Kinds.DE:
                {
                    _AcquireWeapon(player.de);
                    break;
                }
            case Kinds.ingram:
                {
                    _AcquireWeapon(player.ingram);
                    break;
                }
            case Kinds.hk416:
                {
                    _AcquireWeapon(player.hk416);
                    break;
                }
            case Kinds.mk14:
                {
                    _AcquireWeapon(player.mk14);
                    break;
                }
            case Kinds.m249:
                {
                    _AcquireWeapon(player.m249);
                    break;
                }
            case Kinds.pulsegun:
                {
                    _AcquireWeapon(player.pulse);
                    break;
                }
        }

        ChangeSlot(true);
    }

    private static void _AcquireWeapon(GameObject go)
    {
        if (slotList.Contains(go))
        {
            slotListIdx = slotList.IndexOf(go);
        }
        else
        {
            slotList.Add(go);
            slotListIdx = slotList.Count - 1;
        }
    }

    // 숫자 1, 2 키로 무기 변경
    private void ChangeWeapon()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            ChangeSlot(false);

            slotListIdx--;
            if (slotListIdx < 0)
                slotListIdx = slotList.Count - 1;

            ChangeSlot(true);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            ChangeSlot(false);

            slotListIdx++;
            if (slotListIdx == slotList.Count)
                slotListIdx = 0;

            ChangeSlot(true);
        }

    }

    static void ChangeSlot(bool enable)
    {
        if (slotList.Count == 0)
            return;

        // 총
        slotList[slotListIdx].SetActive(enable);

        // UI
        if (enable == true)
        {
            InstSlotUI = Instantiate(slotList[slotListIdx].GetComponent<Weapon>().slotUI);
            InstSlotUI.transform.SetParent(slotUI.transform, false);
            InstSlotUI.GetComponent<UnityEngine.UI.Image>().enabled = enable;
        }
        else
        {
            if (InstSlotUI)
            {
                Destroy(InstSlotUI);
            }
        }
    }

    // EnumeyController.cs에서 Update()에서 호출
    public static Weapon GetWeapon()
    {
        return slotList[slotListIdx].GetComponent<Weapon>();
    }

    public static int GetWeaponIdx()
    {
        try
        {
            return slotList[slotListIdx].GetComponent<Weapon>().idx;
        }
        catch
        {
            return 0;
        }
    }

    public static void TransformWeapon()
    {
        if (player == null)
            player = GameObject.FindObjectOfType<PlayerController>();

        ChangeSlot(false);

        // 장착한 무기 제거
        slotList.RemoveAt(slotListIdx);

        // 현재 가지고 있는 무기 확인
        Kinds has = new Kinds();
        FindWeaponKinds(ref has);

        Kinds anotherWeapon = (Kinds)Mathf.Pow(2, Random.Range(0, 11));
        while ((anotherWeapon & has) == anotherWeapon)
        {
            anotherWeapon = (Kinds)Mathf.Pow(2, Random.Range(0, 11));
        }
        AcquireWeapon(anotherWeapon, false);


    }

    private static void FindWeaponKinds(ref Kinds has)
    {
        foreach (GameObject go in slotList)
        {
            if (go.Equals(player.mp5))
                has = has | Kinds.mp5;
            else if (go.Equals(player.shotgun))
                has = has | Kinds.doubleBarrel;
            else if (go.Equals(player.ak))
                has = has | Kinds.ak;
            else if (go.Equals(player.kar98))
                has = has | Kinds.kar98;
            else if (go.Equals(player.pump))
                has = has | Kinds.pump;
            else if (go.Equals(player.de))
                has = has | Kinds.DE;
            else if (go.Equals(player.ingram))
                has = has | Kinds.ingram;
            else if (go.Equals(player.hk416))
                has = has | Kinds.hk416;
            else if (go.Equals(player.mk14))
                has = has | Kinds.mk14;
            else if (go.Equals(player.m249))
                has = has | Kinds.m249;
            else if (go.Equals(player.pulse))
                has = has | Kinds.pulsegun;
            else
                has = Kinds.pistol;
        }
    }
}