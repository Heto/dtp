﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBox : MonoBehaviour {

    public PlayerController player;
    public PerksManager perks;
    public Animator anim;
    public GameObject agility;
    public GameObject castle;
    public GameObject drug;
    public GameObject greed;
    public GameObject healing;
    public GameObject skeletonKey;
    public GameObject strength;
    public GameObject transformation;
    public GameObject grenade;
    public GameObject claymore;
    public GameObject tankMine;
    public GameObject turret;
    public Vector3 spawnPosition;

    public int price;
    private int randomItem;

    // Use this for initialization
    void Start () {
        player = GameObject.FindObjectOfType<PlayerController>();
        perks = GameObject.Find("Player").GetComponent<PerksManager>();
        spawnPosition = new Vector3(transform.position.x, transform.position.y, -0.1f);
        anim = GetComponent<Animator>();
        if (perks.perk_Bargain)
            price = price / 2;
        else
            price = 50;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E) && player.energy >= price && anim.GetBool("canUse"))
            {
                player.energy -= price;
                if (perks.perk_Bargain)
                    price += (int)(price * 0.5f) / 2;
                else
                    price += (int)(price * 0.5f);
                anim.SetTrigger("use");
            }
        }
    }

    void AnimStart()
    {
        anim.SetBool("canUse", false);
    }


    void AnimEnd()
    {
        anim.SetTrigger("toIdle");
        anim.SetBool("canUse", true);
        if (perks.perk_TurretMaster)
            randomItem = Random.Range(1, 15);
        else
            randomItem = Random.Range(1, 13);
        
        if (randomItem == 1)
        {
            var clone = Instantiate(agility, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 2)
        {
            var clone = Instantiate(castle, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 3)
        {
            var clone = Instantiate(drug, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 4)
        {
            var clone = Instantiate(greed, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 5)
        {
            var clone = Instantiate(healing, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 6)
        {
            var clone = Instantiate(skeletonKey, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 7)
        {
            var clone = Instantiate(strength, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 8)
        {
            var clone = Instantiate(transformation, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 9)
        {
            var clone = Instantiate(grenade, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f , 100f));
        }
        if (randomItem == 10)
        {
            var clone = Instantiate(claymore, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 11)
        {
            var clone = Instantiate(tankMine, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 12)
        {
            var clone = Instantiate(turret, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 13)
        {
            var clone = Instantiate(turret, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 14)
        {
            var clone = Instantiate(turret, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }
        if (randomItem == 15)
        {
            var clone = Instantiate(turret, spawnPosition, transform.rotation);
            clone.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(-100f, 100f));
        }  


    }
}
