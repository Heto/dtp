﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FadeOut : MonoBehaviour {

    public Animator anim;
    public PlayerController player;
    //private float animTime = 2f;
    //public Image fadeImage;

    //private float start = 0f;
    //private float end = 1f;
    //private float time = 0f;

    //private bool isPlaying = false;

    //private void Awake()
    //{
    //    fadeImage = GetComponent<Image>();
    //}
    //public void StartFadeAnim()
    //{
    //    if (isPlaying == true)
    //        return;
    //    StartCoroutine("PlayFadeOut");
    //}
    //IEnumerator PlayFadeOut()
    //{
    //    isPlaying = true;
    //    Color color = fadeImage.color;
    //    time = 0f;
    //    color.a = Mathf.Lerp(start, end, time);
    //    while(color.a < 1f)
    //    {
    //        time += Time.deltaTime / animTime;

    //        color.a = Mathf.Lerp(start, end, time);
    //        for (int i = 0; i < 5; i++)
    //        {
    //            //fadeImage[i].color = color;
    //            fadeImage.color = color;
    //        }
    //        yield return null;
    //    }
    //    isPlaying = false;
    //}

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        anim.SetTrigger("GameOver");
        player.isGameOvered = true;
    }
}
