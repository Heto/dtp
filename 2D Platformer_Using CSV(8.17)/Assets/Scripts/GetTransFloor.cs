﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetTransFloor : MonoBehaviour {

    public Transform barricadeLeftPos;
    public Transform barricadeRightPos;
    public Transform hactchPos;
    public Transform upstairPos;
    public Transform downstairPos;
    public Transform itemboxPos;
    public Transform spawnPointLeft;
    public Transform spawnPointRight;
    public Transform block1;
    public Transform block2;




	// Use this for initialization
	void Start () {
		
	}
	public List<Transform> GetTransList()
    {
        List<Transform> a = new List<Transform>();
        a.Add(barricadeLeftPos);
        a.Add(barricadeRightPos);
        a.Add(hactchPos);
        a.Add(upstairPos);
        a.Add(downstairPos);
        a.Add(itemboxPos);
        a.Add(spawnPointLeft);
        a.Add(spawnPointRight);
        a.Add(block1);
        a.Add(block2);

        return a;
    }
	// Update is called once per frame
	void Update () {
		
	}
}
