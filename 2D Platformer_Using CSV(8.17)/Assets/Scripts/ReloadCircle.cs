﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReloadCircle : MonoBehaviour
{

	public Transform LoadingBar;
	public Image radialProgressBar;
	public Image center;
	public Weapon weapon;
	public PlayerController player;
	private float staticreloadTime;


    public AudioClip reloadSound;
    public AudioClip shogundReloadSound;
    private AudioSource source;

	void Start()
	{
	}

	void Update()
	{
		transform.position = Input.mousePosition;
		player = GameObject.Find("Player").GetComponent<PlayerController>();
        weapon = WeaponSlot.GetWeapon();

        switch (WeaponSlot.GetWeaponIdx())
		{
			case 0:
				staticreloadTime = weapon.reloadtime;
				LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;

				if (weapon.reloadcheck)
				{

                    Cursor.visible = false;
					radialProgressBar.enabled = true;
					center.enabled = true;
				}
				else
				{
					Cursor.visible = true;
					radialProgressBar.enabled = false;
					center.enabled = false;
				}
				break;
			case 1:
				staticreloadTime = weapon.reloadtime;
				LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;
				if (weapon.reloadcheck)
				{
					Cursor.visible = false;
					radialProgressBar.enabled = true;
					center.enabled = true;
				}
				else
				{
					Cursor.visible = true;
					radialProgressBar.enabled = false;
					center.enabled = false;
				}
				break;
			case 2:
				staticreloadTime = weapon.reloadtime;
				LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;
				if (weapon.reloadcheck)
				{
					Cursor.visible = false;
					radialProgressBar.enabled = true;
					center.enabled = true;
				}
				else
				{
					Cursor.visible = true;
					radialProgressBar.enabled = false;
					center.enabled = false;
				}
				break;
			case 3:
				staticreloadTime = weapon.reloadtime;
				LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;
				if (weapon.reloadcheck)
				{
					Cursor.visible = false;
					radialProgressBar.enabled = true;
					center.enabled = true;
				}
				else
				{
					Cursor.visible = true;
					radialProgressBar.enabled = false;
					center.enabled = false;
				}
				break;
			case 4:
				staticreloadTime = weapon.reloadtime;
				LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;
				if (weapon.reloadcheck)
				{
					Cursor.visible = false;
					radialProgressBar.enabled = true;
					center.enabled = true;
				}
				else
				{
					Cursor.visible = true;
					radialProgressBar.enabled = false;
					center.enabled = false;
				}
				break;
			case 5:
				staticreloadTime = weapon.reloadtime;
				LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;
				if (weapon.reloadcheck)
				{
					Cursor.visible = false;
					radialProgressBar.enabled = true;
					center.enabled = true;
				}
				else
				{
					Cursor.visible = true;
					radialProgressBar.enabled = false;
					center.enabled = false;
				}
				break;
            case 6:
                staticreloadTime = weapon.reloadtime;
                LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;
                if (weapon.reloadcheck)
                {
                    Cursor.visible = false;
                    radialProgressBar.enabled = true;
                    center.enabled = true;
                }
                else
                {
                    Cursor.visible = true;
                    radialProgressBar.enabled = false;
                    center.enabled = false;
                }
                break;
            case 7:
                staticreloadTime = weapon.reloadtime;
                LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;
                if (weapon.reloadcheck)
                {
                    Cursor.visible = false;
                    radialProgressBar.enabled = true;
                    center.enabled = true;
                }
                else
                {
                    Cursor.visible = true;
                    radialProgressBar.enabled = false;
                    center.enabled = false;
                }
                break;
            case 8:
                staticreloadTime = weapon.reloadtime;
                LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;
                if (weapon.reloadcheck)
                {
                    Cursor.visible = false;
                    radialProgressBar.enabled = true;
                    center.enabled = true;
                }
                else
                {
                    Cursor.visible = true;
                    radialProgressBar.enabled = false;
                    center.enabled = false;
                }
                break;
            case 9:
                staticreloadTime = weapon.reloadtime;
                LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;
                if (weapon.reloadcheck)
                {
                    Cursor.visible = false;
                    radialProgressBar.enabled = true;
                    center.enabled = true;
                }
                else
                {
                    Cursor.visible = true;
                    radialProgressBar.enabled = false;
                    center.enabled = false;
                }
                break;
            case 10:
                staticreloadTime = weapon.reloadtime;
                LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;
                if (weapon.reloadcheck)
                {
                    Cursor.visible = false;
                    radialProgressBar.enabled = true;
                    center.enabled = true;
                }
                else
                {
                    Cursor.visible = true;
                    radialProgressBar.enabled = false;
                    center.enabled = false;
                }
                break;
            case 11:
                staticreloadTime = weapon.reloadtime;
                LoadingBar.GetComponent<Image>().fillAmount = weapon.reloadStart / staticreloadTime;
                if (weapon.reloadcheck)
                {
                    Cursor.visible = false;
                    radialProgressBar.enabled = true;
                    center.enabled = true;
                }
                else
                {
                    Cursor.visible = true;
                    radialProgressBar.enabled = false;
                    center.enabled = false;
                }
                break;
            default:
				break;
		}
	}
}
