﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitEffect : MonoBehaviour {

    public Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        int i = Random.Range(1, 5);
        switch(i)
        {
            case 1: anim.SetTrigger("hit1"); break;
            case 2: anim.SetTrigger("hit2"); break;
            case 3: anim.SetTrigger("hit3"); break;
            case 4: anim.SetTrigger("hit4"); break;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnEnd()
    {
        Destroy(gameObject);
    }
}
