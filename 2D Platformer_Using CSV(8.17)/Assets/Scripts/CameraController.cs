﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject target;
	public float followAhead;

	private Vector3 targetPosition;

	public float smoothing;

	public bool followTarget;
    public bool bossroomFollowTarget = false;
	// Use this for initialization
	void Start () {
		followTarget = true;
        Screen.SetResolution(320, 180, true);
        //Camera.main.orthographicSize = Screen.height / (100.0f * 2.0f);
        Debug.Log("Screen Resolution sets");
    }
	
	// Update is called once per frame
	void Update () {

		if(followTarget)
		{
            
			targetPosition = new Vector3(target.transform.position.x,target.transform.position.y, transform.position.z);

			transform.position = Vector3.Lerp(transform.position,new Vector3( targetPosition.x,targetPosition.y,targetPosition.z), smoothing * Time.deltaTime);
            
        }
        else if(followTarget==false && bossroomFollowTarget ==true)
        {
            targetPosition = new Vector3(target.transform.position.x, target.transform.position.y, transform.position.z);

            transform.position = Vector3.Lerp(transform.position, new Vector3(targetPosition.x, targetPosition.y + 1.5f, targetPosition.z), smoothing * Time.deltaTime);
        }
    }

}
