﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour {
    public PlayerController player;
    public GameObject mainCamera;
    public GameObject slot;
    public GameObject slotUI;
    public GameObject InstSlotUI;
    private Vector2 dropItemPosition;

    // 생성자
    void Awake()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        slot = GameObject.Find("ItemSlot");
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    // (자식 클래스에서 실행됩니다.)
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                if (player.item != null)
                {
                    dropItemPosition = new Vector2(player.transform.position.x, player.transform.position.y + 0.3f);
                    player.item.transform.position = dropItemPosition;
                    Destroy(player.item.GetComponent<Item>().InstSlotUI); // 현재 가지고 있는 ItemUI 삭제
                }

                gameObject.transform.position = new Vector3(-256, -256, -256);
                player.item = gameObject;
                CarryOn(); // 지속되는 아이템 실행
                // Hierarchy/Canvas/ItemSlot의 위치를 부모로 UI Pref생성
                InstSlotUI = Instantiate(slotUI);
                if (InstSlotUI != null)
                {
                    InstSlotUI.transform.SetParent(slot.transform, false);
                    InstSlotUI.GetComponent<UnityEngine.UI.Image>().enabled = true;
                }

            }
        }
    }

    public virtual void Use() { }

    public virtual void CarryOn() { }

    public virtual void CarryOff() { }

    private void OnDestroy() { }
}
